package com.mediaprofi.zukuzyaSecondEdition.customView;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import java.util.ArrayList;

/**
 * Created by aleksei on 18.12.15.
 */
public class ChoiseDialog extends DialogFragment {


    public ArrayList<String> list;
    DialogInterface.OnClickListener dialogClicker;


    public void setList(ArrayList<String> list) {
        this.list = list;
    }

    public void setClick(DialogInterface.OnClickListener dialogClicker) {
        this.dialogClicker = dialogClicker;
    }

    @Override
    public Dialog onCreateDialog(Bundle saveInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder
                .setSingleChoiceItems(list.toArray(new String[list.size()]), 0, dialogClicker);

        return builder.create();
    }

}
