package com.mediaprofi.zukuzyaSecondEdition.customView;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.mediaprofi.zukuzyaSecondEdition.model.CategoryModel;

import java.util.List;

/**
 * Created by aleksei on 18.12.15.
 */
public class ChoiseDialogCategory extends DialogFragment {

    public List<CategoryModel.Category> list;
    private String[] listTitle;
    private DialogInterface.OnClickListener dialogClicker;

    public void setList(List<CategoryModel.Category> listCategory) {
        listTitle = new String[listCategory.size()];
        for (int i = 0; i < listCategory.size(); i++){
            CategoryModel.Category categorymodel = listCategory.get(i);
            listTitle[i] = categorymodel.getName_kategor();
        }
        this.list = listCategory;
    }

    public void setClick(DialogInterface.OnClickListener dialogClicker) {
        this.dialogClicker = dialogClicker;
    }

    @Override
    public Dialog onCreateDialog(Bundle saveInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder
                .setSingleChoiceItems(listTitle, 0, dialogClicker);

        return builder.create();
    }

}
