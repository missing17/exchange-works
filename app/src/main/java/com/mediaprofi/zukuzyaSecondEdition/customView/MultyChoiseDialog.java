package com.mediaprofi.zukuzyaSecondEdition.customView;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.mediaprofi.zukuzyaSecondEdition.model.PodcategoryModel;
import com.mediaprofi.zukuzyaSecondEdition.ui.registartion.registration.RegistartionActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aleksei on 13.01.16.
 */
public class MultyChoiseDialog extends DialogFragment {

    private List<PodcategoryModel.Podcategory> listPodcategory;
    private RegistartionActivity.MultiChoiseCallBack dialogClicker;
    private String[] showList;
    private boolean[] checked;

    public void setList(List<PodcategoryModel.Podcategory> list) {
        this.listPodcategory = list;
        buildData();
    }

    private void buildData() {
        showList = new String[listPodcategory.size()];
        checked = new boolean[listPodcategory.size()];

        for (int i = 0; i < listPodcategory.size(); i++) {
            PodcategoryModel.Podcategory podcategory = listPodcategory.get(i);
            showList[i] = podcategory.getName_podkategor();
            checked[i] = false;
        }
    }

    public void setClick(RegistartionActivity.MultiChoiseCallBack dialogClicker) {
        this.dialogClicker = dialogClicker;
    }

    @Override
    public Dialog onCreateDialog(Bundle saveInstanceState) {
        final List<Integer> listChecket = new ArrayList<>();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder
                .setMultiChoiceItems(showList, checked, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        if (isChecked)
                            listChecket.add(which);
                        else{
                            for (int i = 0; i< listChecket.size(); i++){
                                int id = listChecket.get(i);
                                if (id == which)
                                    listChecket.remove(i);
                            }
                        }
                    }
                })
        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialogClicker.getChoiseItem(listChecket);
            }
        });

        return builder.create();
    }
}
