package com.mediaprofi.zukuzyaSecondEdition.customView;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.lang.ref.WeakReference;

/**
 * Created by wande_000 on 19.09.2014.
 */
 public class UsPhoneNumberFormatter implements TextWatcher {
    //This TextWatcher sub-class formats entered numbers as 1 (123) 456-7890
    private boolean mFormatting; // this is a flag which prevents the
    // stack(onTextChanged)
    private boolean clearFlag;
    private int mLastStartLocation;
    private String mLastBeforeText;
    private WeakReference<EditText> mWeakEditText;

    public UsPhoneNumberFormatter(WeakReference<EditText> weakEditText) {
        this.mWeakEditText = weakEditText;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count,
                                  int after) {
        if(s.toString().startsWith("+7") && s.length() == 2)
        {
            start = 2;
            mWeakEditText.get().setSelection(start);
        }
        mLastStartLocation = start;
        mLastBeforeText = s.toString();
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before,
                              int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        // Make sure to ignore calls to afterTextChanged caused by the work
        // done below
        if(mWeakEditText.get()!=null &&
                !mWeakEditText.get().getText().toString().startsWith("+7") &&
                mWeakEditText.get().length()<2)
        {
            if(!mFormatting) {
                mWeakEditText.get().setText("+7");
                mWeakEditText.get().setSelection(2);
            }
            return;
        }

        if (!mFormatting) {
            mFormatting = true;

            int curPos = mLastStartLocation;
            String beforeValue = mLastBeforeText;
            String currentValue = s.toString();
            String formattedValue = formatUsNumber(s);
            if (formattedValue.startsWith("+7") && formattedValue.length() == 2)
                mWeakEditText.get().setSelection(2);
            else {
                    if (currentValue.length() > beforeValue.length()) {
                        int setCusorPos = formattedValue.length()
                                - (beforeValue.length() - curPos);
                        mWeakEditText.get().setSelection(setCusorPos < 0 ? 0 : setCusorPos);
                    } else if (currentValue.length() < 18) {
                        int setCusorPos = formattedValue.length()
                                - (currentValue.length() - curPos);
                        if (setCusorPos > 0 && !Character.isDigit(formattedValue.charAt(setCusorPos - 1))) {
                            setCusorPos--;
                        }
                        mWeakEditText.get().setSelection(setCusorPos < 0 ? 0 : setCusorPos);
                    }
            }
            mFormatting = false;
        }

    }

    private String formatUsNumber(Editable text) {
        StringBuilder formattedString = new StringBuilder();
        // Remove everything except digits
        int p = 0;
        int plusSevenIndexOf = text.toString().indexOf("+7");
        if(plusSevenIndexOf != -1)
            text.delete(plusSevenIndexOf,plusSevenIndexOf + 2);
        else
        {
            if(text.length() == 3)
            {
                int index = text.toString().indexOf("+");
                text.delete(index,index + 1);
                index = text.toString().indexOf("7");
                text.delete(index,index + 1);
            }
        }
        while (p < text.length()) {
            char ch = text.charAt(p);
            if (!Character.isDigit(ch)) {
                text.delete(p, p + 1);
            } else {
                p++;
            }
        }
        // Now only digits are remaining
        String allDigitString = text.toString();

        int totalDigitCount = allDigitString.length();

        if (totalDigitCount == 0
                || totalDigitCount > 10
                ) {
            // May be the total length of input length is greater than the
            // expected value so we'll remove all formatting
            if(!allDigitString.startsWith("+7"))
                formattedString
                        .append("+7")
                        .append(allDigitString);
            text.clear();
            text.append(formattedString.toString());
            return formattedString.toString();
        }
        int alreadyPlacedDigitCount = 0;
        // The first 3 numbers beyond '1' must be enclosed in brackets "()"
        if (totalDigitCount - alreadyPlacedDigitCount > 3) {
            formattedString.append("("
                    + allDigitString.substring(alreadyPlacedDigitCount,
                    alreadyPlacedDigitCount + 3) + ") ");
            alreadyPlacedDigitCount += 3;
        }
        // There must be a '-' inserted after the next 3 numbers
        if (totalDigitCount - alreadyPlacedDigitCount > 3) {
            formattedString.append(allDigitString.substring(
                    alreadyPlacedDigitCount, alreadyPlacedDigitCount + 3)
                    + "-");
            alreadyPlacedDigitCount += 3;
        }

        if (totalDigitCount > 5 && totalDigitCount - alreadyPlacedDigitCount > 2) {
            formattedString.append(allDigitString.substring(
                    alreadyPlacedDigitCount, alreadyPlacedDigitCount + 2)
                    + "-");
            alreadyPlacedDigitCount += 2;
        }



        // All the required formatting is done so we'll just copy the
        // remaining digits.
        if (totalDigitCount > alreadyPlacedDigitCount) {
            formattedString.append(allDigitString
                    .substring(alreadyPlacedDigitCount));
        }
        if(!formattedString.toString().startsWith("+7"))
            formattedString.insert(0,"+7");
        text.clear();
        text.append(formattedString.toString());
        return formattedString.toString();
    }

    public static String deformatUsNumber(Editable text)
    {
        return text.toString().substring(2).replace("-", " ").replace(" ", "").replace("(", "").replace(")", "");
    }

}