package com.mediaprofi.zukuzyaSecondEdition.customView;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.InputFilter;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.mediaprofy.zukuzyaSecondEdition.R;

/**
 * Created by aleksei on 11.12.15.
 */
public class CustomFiled extends RelativeLayout {

    private Context mContext;
    private ImageView imgTitle;
    private EditText edtFields;

    public CustomFiled(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        initComponent(context, attrs);
    }

    public CustomFiled(Context context) {
        super(context);
        this.mContext = context;
    }

    private void initComponent(Context context, AttributeSet attrs){
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.custom_field, this);

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CustomFiled, 0, 0);

        Drawable icon = ta.getDrawable(R.styleable.CustomFiled_iconLogo);
        imgTitle = (ImageView)findViewById(R.id.imgTitle);
        imgTitle.setImageDrawable(icon);

        String hint = ta.getString(R.styleable.CustomFiled_hint);
        edtFields = (EditText)findViewById(R.id.edtFields);
        edtFields.setHint(hint);

        String text = ta.getString(R.styleable.CustomFiled_preText);
        edtFields.setText(text);

        Boolean isPassword = ta.getBoolean(R.styleable.CustomFiled_isPassword, false);
        if (isPassword)
            edtFields.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

        Boolean isPhone = ta.getBoolean(R.styleable.CustomFiled_isPhone, false);
        if (isPhone) {
            edtFields.setInputType(InputType.TYPE_CLASS_PHONE);

            int maxLength = 17;
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(maxLength);
            edtFields.setFilters(FilterArray);
        }
    }

    public String getEntreText(){
        return edtFields.getText().toString();
    }

    public void setEntreText(String text){
        edtFields.setText(text);
    }

    public void setEdtFieldsLsnr(OnClickListener cliskListener){
        edtFields.setOnClickListener(cliskListener);
    }

    public EditText getEditTextView(){
        return edtFields;
    }
}
