package com.mediaprofi.zukuzyaSecondEdition.customView;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mediaprofy.zukuzyaSecondEdition.R;

/**
 * Created by aleksei on 11.12.15.
 */
public class CustomFiledSpinner extends RelativeLayout {

    private Typeface font;
    private Context mContext;
    private ImageView imgTitle;
    private TextView spnrFields;
    private OnClickListener clickListener;

    public CustomFiledSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        font = Typeface.createFromAsset(context.getAssets(), "fontawesome-webfont.ttf");
        initComponent(context, attrs);
    }

    public void setClick(OnClickListener clickListener){
        this.clickListener = clickListener;
    }

    public CustomFiledSpinner(Context context) {
        super(context);
        this.mContext = context;
    }

    private void initComponent(Context context, AttributeSet attrs){
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.custom_field_select, this);

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CustomFiled, 0, 0);


        Drawable icon = ta.getDrawable(R.styleable.CustomFiled_iconLogo);
        imgTitle = (ImageView)findViewById(R.id.imgTitle);
        imgTitle.setImageDrawable(icon);

        String hint = ta.getString(R.styleable.CustomFiled_hint);
        spnrFields = (TextView)findViewById(R.id.spnrFields);
        spnrFields.setClickable(false);
        spnrFields.setHint(hint);

        TextView caret_down = (TextView)findViewById(R.id.caret_down);
        caret_down.setTypeface(font);

        Boolean isShowImg = ta.getBoolean(R.styleable.CustomFiled_isShowImg, true);
        if (!isShowImg){
            caret_down.setVisibility(GONE);
        }
    }

    public void setTextOnTextView(String text){
        spnrFields.setText(text);
    }

    public String getTextOnTextView(){
        return spnrFields.getText().toString();
    }


}
