package com.mediaprofi.zukuzyaSecondEdition.customView;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.mediaprofi.zukuzyaSecondEdition.model.CityModel;

import java.util.List;

/**
 * Created by aleksei on 18.12.15.
 */
public class ChoiseDialogCity extends DialogFragment {

    public List<CityModel.City> list;
    private String[] listTitle;
    private DialogInterface.OnClickListener dialogClicker;

    public void setList(List<CityModel.City> listCategory) {
        listTitle = new String[listCategory.size()];
        for (int i = 0; i < listCategory.size(); i++){
            CityModel.City citymodel = listCategory.get(i);
            listTitle[i] = citymodel.getName();
        }
        this.list = listCategory;
    }

    public void setClick(DialogInterface.OnClickListener dialogClicker) {
        this.dialogClicker = dialogClicker;
    }

    @Override
    public Dialog onCreateDialog(Bundle saveInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder
                .setSingleChoiceItems(listTitle, 0, dialogClicker);

        return builder.create();
    }

}
