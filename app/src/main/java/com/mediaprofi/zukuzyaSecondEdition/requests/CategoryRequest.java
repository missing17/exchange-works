package com.mediaprofi.zukuzyaSecondEdition.requests;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.json.jackson.JacksonFactory;
import com.mediaprofi.zukuzyaSecondEdition.core.Constants;
import com.mediaprofi.zukuzyaSecondEdition.model.CategoryModel;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

/**
 * Created by aleksei on 12.01.16.
 */
public class CategoryRequest extends GoogleHttpClientSpiceRequest<CategoryModel> {

    public CategoryRequest() {
        super(CategoryModel.class);
    }

    @Override
    public CategoryModel loadDataFromNetwork() throws Exception {
        String url = Constants.baseURL + "/category.php";
        HttpRequest request = getHttpRequestFactory()
                .buildGetRequest(new GenericUrl(url));

        request.setParser(new JacksonFactory().createJsonObjectParser());
        HttpResponse response = request.execute();
        return response.parseAs(getResultType());
    }
}
