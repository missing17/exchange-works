package com.mediaprofi.zukuzyaSecondEdition.requests;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.json.jackson.JacksonFactory;
import com.mediaprofi.zukuzyaSecondEdition.core.Constants;
import com.mediaprofi.zukuzyaSecondEdition.model.UserModel;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

/**
 * Created by aleksei on 11.01.16.
 */
public class UserInfoRequest extends GoogleHttpClientSpiceRequest<UserModel> {

    private String token;
    public UserInfoRequest(String token) {
        super(UserModel.class);
        this.token = token;
    }

    @Override
    public UserModel loadDataFromNetwork() throws Exception {
            String url = Constants.baseURL + "/data_json.php?user_info=1";
            HttpRequest request = getHttpRequestFactory()
                    .buildGetRequest(new GenericUrl(url))
                    .setHeaders(new HttpHeaders()
                            .set("X-Token", token));
            request.setParser(new JacksonFactory().createJsonObjectParser());
            HttpResponse reponse = request.execute();
            return reponse.parseAs(getResultType());
    }
}
