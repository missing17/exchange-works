package com.mediaprofi.zukuzyaSecondEdition.requests;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.json.jackson.JacksonFactory;
import com.mediaprofi.zukuzyaSecondEdition.core.Constants;
import com.mediaprofi.zukuzyaSecondEdition.model.CityModel;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

/**
 * Created by aleksei on 19.01.16.
 */
public class CityRequest extends GoogleHttpClientSpiceRequest<CityModel>{

    public CityRequest() {
        super(CityModel.class);
    }

    @Override
    public CityModel loadDataFromNetwork() throws Exception {
        String url = Constants.baseURL + "/city.php ";
        HttpRequest request = getHttpRequestFactory()
                .buildGetRequest(new GenericUrl(url));

        request.setParser(new JacksonFactory().createJsonObjectParser());
        HttpResponse response = request.execute();
        return response.parseAs(getResultType());
    }
}
