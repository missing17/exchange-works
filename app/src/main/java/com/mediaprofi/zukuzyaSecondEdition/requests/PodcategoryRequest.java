package com.mediaprofi.zukuzyaSecondEdition.requests;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.json.jackson.JacksonFactory;
import com.mediaprofi.zukuzyaSecondEdition.core.Constants;
import com.mediaprofi.zukuzyaSecondEdition.model.PodcategoryModel;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

/**
 * Created by aleksei on 16.01.16.
 */
public class PodcategoryRequest extends GoogleHttpClientSpiceRequest<PodcategoryModel> {

    private String idCategory;

    public PodcategoryRequest(String idCategory) {
        super(PodcategoryModel.class);
        this.idCategory = idCategory;
    }

    @Override
    public PodcategoryModel loadDataFromNetwork() throws Exception {
        String url = Constants.baseURL + "/subcategory.php?kateg=" + idCategory;
        HttpRequest request = getHttpRequestFactory()
                .buildGetRequest(new GenericUrl(url));

        request.setParser(new JacksonFactory().createJsonObjectParser());
        HttpResponse response = request.execute();
        return response.parseAs(getResultType());
    }
}
