package com.mediaprofi.zukuzyaSecondEdition.requests;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.json.jackson.JacksonFactory;
import com.mediaprofi.zukuzyaSecondEdition.core.Constants;
import com.mediaprofi.zukuzyaSecondEdition.model.TokenModel;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

/**
 * Created by aleksei on 06.01.16.
 */
public class TokenRequest extends GoogleHttpClientSpiceRequest<TokenModel> {

    private String login;
    private String password;

    public TokenRequest(String login, String password) {
        super(TokenModel.class);
        this.login = login;
        this.password = password;
    }

    @Override
    public TokenModel loadDataFromNetwork() throws Exception {
        String url = Constants.baseURL + "/auth.php?user=" + login + "&passwd=" + password;
        HttpRequest request = getHttpRequestFactory()
                .buildGetRequest(new GenericUrl(url));
        request.setParser(new JacksonFactory().createJsonObjectParser());
        HttpResponse reponse = request.execute();
        return reponse.parseAs(getResultType());
    }
}
