package com.mediaprofi.zukuzyaSecondEdition;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.mediaprofi.zukuzyaSecondEdition.core.App;
import com.mediaprofi.zukuzyaSecondEdition.core.BaseSpaceActivity;
import com.mediaprofi.zukuzyaSecondEdition.core.Constants;
import com.mediaprofi.zukuzyaSecondEdition.customView.ChoiseDialogCity;
import com.mediaprofi.zukuzyaSecondEdition.customView.CustomFiledSpinner;
import com.mediaprofi.zukuzyaSecondEdition.gcm.RegistrationIntentService;
import com.mediaprofi.zukuzyaSecondEdition.model.CityModel;
import com.mediaprofi.zukuzyaSecondEdition.model.UserModel;
import com.mediaprofi.zukuzyaSecondEdition.requests.CityRequest;
import com.mediaprofi.zukuzyaSecondEdition.requests.UserInfoRequest;
import com.mediaprofi.zukuzyaSecondEdition.ui.order.OredInDistrictActivity;
import com.mediaprofi.zukuzyaSecondEdition.ui.district.adapter.DistrictOrderAdapter;
import com.mediaprofi.zukuzyaSecondEdition.ui.district.decoration.DistrictGridDecoration;
import com.mediaprofi.zukuzyaSecondEdition.ui.district.model.DistrictModel;
import com.mediaprofi.zukuzyaSecondEdition.ui.district.request.DistrictRequest;
import com.mediaprofi.zukuzyaSecondEdition.ui.myorder.MyOrderActivity;
import com.mediaprofi.zukuzyaSecondEdition.ui.settings.Setting;
import com.mediaprofi.zukuzyaSecondEdition.ui.settings.problem.ProblemActivity;
import com.mediaprofi.zukuzyaSecondEdition.ui.ChoiseTariff;
import com.mediaprofi.zukuzyaSecondEdition.ui.profile.Profile;
import com.mediaprofy.zukuzyaSecondEdition.R;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aleksei on 01.12.15.
 */
public class MainActivity extends BaseSpaceActivity implements IClickOnMyOrder, SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout swipe_container;
    private SharedPreferences defaultSharedPreferences;
    private int idCityUser = 0;
    private String token;
    private List<DistrictModel.District> districtList;
    private RecyclerView myRecyclerView;
    private DistrictOrderAdapter adapter;
    private CustomFiledSpinner city;
    private ChoiseDialogCity dialogCity;
    private RelativeLayout rlWaite;
    private RelativeLayout rlNoData;
    private TextView txtNotyfication;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_screen);

        buildActionBar(R.string.select_district, false, true);

        defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (!defaultSharedPreferences.getBoolean(Constants.sendTokenOnServerSharedPreferences, false)) {
            if (checkPlayServices()) {
                Intent intent = new Intent(MainActivity.this, RegistrationIntentService.class);
                startService(intent);
            }
        }


        token = getGloabalSetting().getString(Constants.tokenInSharedPreferences, "");
        districtList = new ArrayList<>();

        city = (CustomFiledSpinner) findViewById(R.id.city);

        rlWaite = (RelativeLayout) findViewById(R.id.rlWaite);
        rlWaite.setVisibility(View.VISIBLE);

        rlNoData = (RelativeLayout) findViewById(R.id.rlNoData);
        txtNotyfication = (TextView) findViewById(R.id.txtNotyfication);

        myRecyclerView = (RecyclerView) findViewById(R.id.districtList);

        myRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        myRecyclerView.addItemDecoration(new DistrictGridDecoration());

        adapter = new DistrictOrderAdapter(districtList, this, this);
        myRecyclerView.setAdapter(adapter);

        swipe_container = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipe_container.setOnRefreshListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        UserModel userModel = (UserModel) App.getFromMemoryCache(Constants.keyUser);
        if (userModel == null) {
            UserInfoRequest userInfoRequest = new UserInfoRequest(getGloabalSetting().getString(Constants.tokenInSharedPreferences, ""));
            getSpiceManager().execute(userInfoRequest, new UserInfoRequestListener());
        } else {
            idCityUser = (int)App.getFromMemoryCache(Constants.keyCityUerSelected);//Integer.valueOf(userModel.getCity_id());
            makeDrawableMenu(userModel);
            checkDataForFragment();
        }
    }

    private void checkDataForFragment() {
        List<String> lostData = new ArrayList<>();

        if (checkCity()) {
            lostData.add("city");
        } else {
            setCustomFiledCity((List<CityModel.City>) App.getFromMemoryCache(Constants.keyCity));
        }

        if (districtList == null || districtList.size() == 0) {
            lostData.add("district");
        }

        if (lostData.size() != 0)
            loadDataFromServer(lostData);

        endNetworkRequest(true, null);
    }


    private boolean checkCity() {
        List<CityModel.City> listCity = (List<CityModel.City>) App.getFromMemoryCache(Constants.keyCity);
        return (listCity == null || listCity.size() == 0);
    }

    private void loadDataFromServer(List<String> lostData) {
        for (String strError : lostData) {
            if (strError.equals("city"))
                goOnServerByCity();
            if (strError.equals("district"))
                goOnServerByDistrictList(new DistrictRequest(idCityUser, token));
        }
    }

    private void goOnServerByDistrictList(DistrictRequest requests) {
        getSpiceManager().execute(requests, new listDistrictModel());
    }

    private void goOnServerByCity() {
        CityRequest cityRequest = new CityRequest();
        getSpiceManager().execute(cityRequest, new CityRequestListener());
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!defaultSharedPreferences.getBoolean(Constants.sendTokenOnServerSharedPreferences, false)) {
            if (checkPlayServices()) {
                Intent intent = new Intent(MainActivity.this, RegistrationIntentService.class);
                startService(intent);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            getFragmentManager().popBackStack();
        }

        return super.onOptionsItemSelected(item);
    }

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onRefresh() {
        goOnServerByDistrictList(new DistrictRequest(idCityUser, token));
    }

    private class UserInfoRequestListener implements RequestListener<UserModel> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            Log.e("de", "sdw");
        }

        @Override
        public void onRequestSuccess(UserModel userModel) {

            App.putInMemoryCache(Constants.keyUser, userModel);

            idCityUser = Integer.valueOf(userModel.getCity_id());
            App.putInMemoryCache(Constants.keyCityUerSelected, idCityUser);

            SharedPreferences.Editor editor = getGloabalSetting().edit();
            editor.putInt(Constants.cityInSharedPreferences, Integer.valueOf(userModel.getCity_id()));
            editor.apply();
            checkDataForFragment();
            makeDrawableMenu(userModel);
        }
    }

    private class CityRequestListener implements RequestListener<CityModel> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            endNetworkRequest(false, getString(R.string.no_data));
        }

        @Override
        public void onRequestSuccess(CityModel cityModel) {
            //записываем список городов
            App.putInMemoryCache(Constants.keyCity, cityModel.getCity());
            setCustomFiledCity(cityModel.getCity());
        }
    }

    private void setCustomFiledCity(final List<CityModel.City> listCitys) {
        String nameCity = "";

        for (int i = 0; i < listCitys.size(); i++) {
            CityModel.City tmpCity = listCitys.get(i);
            if (idCityUser == Integer.valueOf(tmpCity.getId()))
                nameCity = tmpCity.getName();
        }

        UserModel userModel = (UserModel) App.getFromMemoryCache(Constants.keyUser);
        userModel.setCateg_id(String.valueOf(idCityUser));

        App.putInMemoryCache(Constants.keyUser, userModel);

        city.setTextOnTextView(nameCity);
        city.setOnClickListener(new View.OnClickListener() {

            DialogInterface.OnClickListener clikc = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    selectCity(dialog, which, listCitys);
                }
            };

            @Override
            public void onClick(View v) {
                dialogCity = new ChoiseDialogCity();
                dialogCity.setList(listCitys);
                dialogCity.setClick(clikc);
                dialogCity.show(getFragmentManager(), "Tag");

            }
        });
    }

    private void selectCity(DialogInterface dialog, int which, List<CityModel.City> listCitys) {
        idCityUser = Integer.valueOf(listCitys.get(which).getId());
        App.putInMemoryCache(Constants.keyCityUerSelected, idCityUser);
        city.setTextOnTextView(listCitys.get(which).getName());
        dialog.dismiss();

        DistrictRequest districtRequest = new DistrictRequest(Integer.valueOf(listCitys.get(which).getId()), token);
        goOnServerByDistrictList(districtRequest);
        rlWaite.setVisibility(View.VISIBLE);
    }

    private void endNetworkRequest(boolean sucess, String text) {
        if (sucess) {
            rlNoData.setVisibility(View.GONE);
            rlWaite.setVisibility(View.GONE);
            myRecyclerView.setVisibility(View.VISIBLE);
        } else {
            rlNoData.setVisibility(View.VISIBLE);
            rlWaite.setVisibility(View.GONE);
            myRecyclerView.setVisibility(View.GONE);
            txtNotyfication.setText(text);
        }
    }


    private class listDistrictModel implements RequestListener<DistrictModel> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            endNetworkRequest(false, getString(R.string.no_data));
            swipe_container.setRefreshing(false);
        }

        @Override
        public void onRequestSuccess(DistrictModel districtModel) {

            districtList = districtModel.getCity();
            if (districtList != null && districtList.size() != 0) {
                endNetworkRequest(true, null);
                adapter.updateDateAdapters(districtModel.getCity());
                adapter.notifyDataSetChanged();
            } else {
                endNetworkRequest(false, getString(R.string.data_empty));
            }
            swipe_container.setRefreshing(false);
        }

    }

    private void makeDrawableMenu(UserModel userModel) {

        PrimaryDrawerItem area = new PrimaryDrawerItem().withName("Выбор района").withIcon(R.drawable.ic_district_gary);
        PrimaryDrawerItem myOreder = new PrimaryDrawerItem().withName("Мои заказы").withIcon(R.drawable.ic_account_gray);
        PrimaryDrawerItem settings = new PrimaryDrawerItem().withName("Настройки").withIcon(R.drawable.ic_settings_gray);
        PrimaryDrawerItem info = new PrimaryDrawerItem().withName("Справка").withIcon(R.drawable.ic_information_gary);

//        SecondaryDrawerItem balance = new SecondaryDrawerItem().withName("Баланс: 100 руб.");
//        SecondaryDrawerItem addFunds = new SecondaryDrawerItem().withName("Пополнить счет");
        SecondaryDrawerItem tarif = new SecondaryDrawerItem().withName("Выбрать тариф");

        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.buckground_navigator_menu)
                .withSelectionListEnabled(false)
                .withOnAccountHeaderProfileImageListener(new AccountHeader.OnAccountHeaderProfileImageListener() {
                    @Override
                    public boolean onProfileImageClick(View view, IProfile profile, boolean current) {
                        Intent profileIntent = new Intent(MainActivity.this, Profile.class);
                        startActivity(profileIntent);
//                        Profile profileFragment = new Profile();
//                        replace(profileFragment, "profile");
//                        updateToolBar(R.string.profile);
                        return false;
                    }

                    @Override
                    public boolean onProfileImageLongClick(View view, IProfile profile, boolean current) {
                        return false;
                    }
                })
                .addProfiles(
                        new ProfileDrawerItem()
                                .withName(userModel.getName())
                                .withEmail(userModel.getPhone())
                                .withIcon(Constants.baseURLShort + userModel.getPhoto())
                ).build();

        Drawer drawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(myToolbar)
                .withActionBarDrawerToggle(true)
                .withAccountHeader(headerResult, true)
                .addDrawerItems(
                        area,
                        myOreder,
                        settings,
                        info,
                        new DividerDrawerItem(),
                        tarif
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        switch (position) {
                            case 0:
                                break;
                            case 1:
                                Intent myOrderIntent = new Intent(MainActivity.this, MyOrderActivity.class);// = new MyOrderActivity();
                                startActivity(myOrderIntent);
                                break;
                            case 2:
                                Intent settings = new Intent(MainActivity.this, Setting.class);
                                startActivity(settings);
                                break;
                            case 3:
                                Intent problem = new Intent(MainActivity.this, ProblemActivity.class);
                                startActivity(problem);
                                break;
                            case 5:
                                Intent choiseTariff = new Intent(MainActivity.this, ChoiseTariff.class);
                                startActivity(choiseTariff);
                                break;
                        }
                        return false;
                        // do something with the clicked item :D
                    }
                })
                .build();

        drawer.addStickyFooterItem(new PrimaryDrawerItem().withName(userModel.getCity()).withIcon(R.drawable.ic_map_marker_gray));

    }

    @Override
    public void clickMyOrder(int id, String title) {
        Intent orderInDistrict = new Intent(this, OredInDistrictActivity.class);
        orderInDistrict.putExtra(Constants.position, id);
        orderInDistrict.putExtra(Constants.title, title);
        startActivity(orderInDistrict);
    }
}
