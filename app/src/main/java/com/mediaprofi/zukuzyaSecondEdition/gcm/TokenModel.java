package com.mediaprofi.zukuzyaSecondEdition.gcm;

import com.google.api.client.util.Key;

/**
 * Created by aleksei on 25.01.16.
 */
public class TokenModel {
//    @SerializedName("push")
//    @Expose
//    private String push;
//
//    public String getPush() {
//        return push;
//    }
//
//    public void setPush(String push) {
//        this.push = push;
//    }

    @Key
    private String push;

    public String getPush() {
        return push;
    }

    public void setPush(String push) {
        this.push = push;
    }
}
