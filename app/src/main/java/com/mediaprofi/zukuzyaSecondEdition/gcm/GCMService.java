package com.mediaprofi.zukuzyaSecondEdition.gcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;
import com.mediaprofy.zukuzyaSecondEdition.R;
import com.mediaprofi.zukuzyaSecondEdition.core.Constants;
import com.mediaprofi.zukuzyaSecondEdition.ui.description.DescriptionOrder;

/**
 * Created by aleksei on 06.01.16.
 */
public class GCMService extends GcmListenerService {
    @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = data.getString("message");
        Log.d("GSMService", "From: " + from);
        Log.d("GSMService", "Message: " + message);

//        if (from.startsWith("/topics/")) {
//            // message received from some topic.
//        } else {
//            // normal downstream message.
//        }
        SharedPreferences sharedPreferences = getSharedPreferences(Constants.nameSharedPrefernces, MODE_PRIVATE);
        boolean showNotification = sharedPreferences.getBoolean(Constants.notificationImSharedPreferences, true);
        if (showNotification)
            sendNotification(data);

    }

    private void sendNotification(Bundle data) {
        String messageTitle = data.getString("title");
        String messageBody = data.getString("text");
        String messageType = data.getString("id");
        boolean isOpen = data.getBoolean("isOpen");

        Intent intent = new Intent(this, DescriptionOrder.class);

//        Bundle bundle = new Bundle();
        intent.putExtra(Constants.positionOrder, Integer.valueOf(messageType));
        intent.putExtra(Constants.isOpenOrder, isOpen);
//        bundle.putInt(Constants.positionOrder, messageType);
//        bundle.putBoolean(Constants.isOpenOrder, isOpen);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        intent.putExtra("Info", bundle);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);


        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_information_gary)
                .setContentTitle(messageTitle)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }
}
