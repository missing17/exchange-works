package com.mediaprofi.zukuzyaSecondEdition.gcm;

import android.content.Intent;

import com.google.android.gms.iid.InstanceIDListenerService;

/**
 * Created by aleksei on 06.01.16.
 */
public class InstanseId extends InstanceIDListenerService {

    @Override
    public void onTokenRefresh(){
        Intent intent = new Intent(this, RegistrationIntentService.class);
        startService(intent);
    }
}
