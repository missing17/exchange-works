package com.mediaprofi.zukuzyaSecondEdition.gcm.network;

import com.mediaprofi.zukuzyaSecondEdition.gcm.TokenModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Query;


/**
 * Created by aleksei on 06.04.16.
 */
public interface ApiInterface {

    @GET("http://zukusya.com/ajax_scripts/push_on.php")
    @Headers("Content-Type: application/json")
    Call<TokenModel> sendPushKey(@Header("X-Token") String token , @Query("user_push") String push);
}
