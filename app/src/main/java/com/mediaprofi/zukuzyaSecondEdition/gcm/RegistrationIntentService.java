package com.mediaprofi.zukuzyaSecondEdition.gcm;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.mediaprofi.zukuzyaSecondEdition.gcm.network.ApiInterface;
import com.mediaprofy.zukuzyaSecondEdition.R;
import com.mediaprofi.zukuzyaSecondEdition.core.Constants;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by aleksei on 07.01.16.
 */
public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegIntentService";
    private static final String[] TOPICS = {"global"};
    private SharedPreferences sharedPreferencesDefault;

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        sharedPreferencesDefault = PreferenceManager.getDefaultSharedPreferences(this);
        try {
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);

            sendRegistrationToServer(token);

            subscribeTopics(token);

        } catch (Exception e) {
            Log.e("Service", e.toString());
        }

        Intent registrationComplete = new Intent(Constants.REGISTRATION_COMPLETE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void sendRegistrationToServer(String key) {

        SharedPreferences sharedPreferences = getApplication().getSharedPreferences(Constants.nameSharedPrefernces, MODE_PRIVATE);
        String tokenUser = sharedPreferences.getString(Constants.tokenInSharedPreferences, "");

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.baseURL + "/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Log.e("Terves --- frfrf", "Создал запрос");
        Log.d("Terves --- frfrf", "Создал запрос");

        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        Call<TokenModel> request = apiInterface.sendPushKey(tokenUser, key);

        try {
            Response<TokenModel> tokenModel = request.execute();
            Log.e("Terves --- frfrf", tokenModel.toString());
        } catch (IOException e) {
            Log.e("Terves ---dedede", e.toString());
            e.printStackTrace();
        }

//        SpiceManager spiceManager2 = new SpiceManager(MultiThreadedGoogleHttpClientSpiceService.class);
//        SendTokenRequest sendTokenRequest = new SendTokenRequest(tokenUser, key);
//
//        spiceManager2.execute(sendTokenRequest, new TokenPushListener());

    }

    private void subscribeTopics(String token) throws IOException {
//        GcmPubSub pubSub = GcmPubSub.getInstance(this);
//        for (String topic : TOPICS) {
//            pubSub.subscribe(token, "/topics/" + topic, null);
//        }
    }

    private void saveTokenInmemory() {
        sharedPreferencesDefault.edit().putBoolean(Constants.sendTokenOnServerSharedPreferences, true).apply();
    }

    private class TokenPushListener implements RequestListener<TokenModel> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {

        }

        @Override
        public void onRequestSuccess(TokenModel tokenModel) {
            if (tokenModel.getPush().equals("yes"))
                saveTokenInmemory();
        }
    }


}
