package com.mediaprofi.zukuzyaSecondEdition.gcm;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.json.jackson.JacksonFactory;
import com.mediaprofi.zukuzyaSecondEdition.core.Constants;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

/**
 * Created by aleksei on 25.01.16.
 */
public class SendTokenRequest extends GoogleHttpClientSpiceRequest<TokenModel> {

    // "api key" = AIzaSyC4znDLPyZbVKi1u9d68mok4PV9JDZv4q8;


//   AIzaSyDh7S8gl49_gVyud9R2Uo6yKB6FbB_U8j4
    // 142272250543

    //AIzaSyDIb2sVHP6xs5N2EB1i9QPIdlGkxj8EFFk
    //235437823468
    private String token;
    private String apiKey;
        public SendTokenRequest(String token, String apiKey) {
            super(TokenModel.class);
            this.token = token;
            this.apiKey = apiKey;
        }

    @Override
    public TokenModel loadDataFromNetwork() throws Exception {
        String url = Constants.baseURL + "/push_on.php?user_push=" + apiKey;
        HttpRequest request = getHttpRequestFactory()
                .buildGetRequest(new GenericUrl(url))
                .setHeaders(new HttpHeaders()
                        .set("X-Token", token));

        request.setParser(new JacksonFactory().createJsonObjectParser());
        HttpResponse response = request.execute();
        return response.parseAs(getResultType());
    }
}
