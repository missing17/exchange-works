package com.mediaprofi.zukuzyaSecondEdition.gcm;

import com.octo.android.robospice.JacksonGoogleHttpClientSpiceService;

/**
 * Created by aleksei on 25.01.16.
 */
public class MultiThreadedGoogleHttpClientSpiceService extends JacksonGoogleHttpClientSpiceService {

    @Override
    public int getThreadCount() {
        return 2;
    }
}
