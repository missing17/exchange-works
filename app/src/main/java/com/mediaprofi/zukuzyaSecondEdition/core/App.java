package com.mediaprofi.zukuzyaSecondEdition.core;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.mediaprofy.zukuzyaSecondEdition.R;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.materialdrawer.util.AbstractDrawerImageLoader;
import com.mikepenz.materialdrawer.util.DrawerImageLoader;
import com.mikepenz.materialdrawer.util.DrawerUIUtils;

import org.acra.ReportField;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import java.util.HashMap;

/**
 * Created by aleksei on 12.01.16.
 */


public class App extends Application {

    private static HashMap<String, Object> mMemoryCache;
    private static SharedPreferences mSharedPreferences;

    @Override
    public void onCreate() {
        super.onCreate();

//        ACRA.init(this);

        mMemoryCache = new HashMap<>();
        mSharedPreferences = getSharedPreferences(Constants.nameSharedPrefernces, MODE_PRIVATE);

        DrawerImageLoader.init(new AbstractDrawerImageLoader() {
            @Override
            public void set(ImageView imageView, Uri uri, Drawable placeholder) {
                Glide.with(imageView.getContext()).load(uri).placeholder(placeholder).into(imageView);
            }

            @Override
            public void cancel(ImageView imageView) {
                Glide.clear(imageView);
            }

            @Override
            public Drawable placeholder(Context ctx, String tag) {
                if (DrawerImageLoader.Tags.PROFILE.name().equals(tag)) {
                    return DrawerUIUtils.getPlaceHolder(ctx);
                } else if (DrawerImageLoader.Tags.ACCOUNT_HEADER.name().equals(tag)) {
                    return new IconicsDrawable(ctx).iconText(" ").backgroundColorRes(com.mikepenz.materialdrawer.R.color.primary).sizeDp(56);
                } else if ("customUrlItem".equals(tag)) {
                    return new IconicsDrawable(ctx).iconText(" ").backgroundColorRes(R.color.md_red_500).sizeDp(56);
                }
                return super.placeholder(ctx, tag);
            }
        });
    }

    public static SharedPreferences getGlobalParameters(){
        return mSharedPreferences;
    }

    public static void putInMemoryCache(String key, Object value){
        mMemoryCache.put(key, value);
    }

    public static Object getFromMemoryCache(String key){
        return mMemoryCache.get(key);
    }



//    Server API Key help
//    AIzaSyC1R7mWVleEXzLZ9VtRiytdpEq_-X3JR8w

//    Sender ID: help
//    775494098912
}
