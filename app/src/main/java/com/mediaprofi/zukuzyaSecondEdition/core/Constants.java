package com.mediaprofi.zukuzyaSecondEdition.core;

/**
 * Created by aleksei on 16.12.15.
 */
public class Constants {

    public static final String baseURL = "http://zukusya.com/ajax_scripts";
    public static final String baseURLShort = "http://zukusya.com/";

    public static final String nameSharedPrefernces = "ExchangeWorkData";

    public static final String cityInSharedPreferences = "City";
    public static final String notificationImSharedPreferences = "notify";
    public static final String tokenInSharedPreferences = "token";
    public static final String sendTokenOnServerSharedPreferences = "regToken";

    public static final String positionOrder = "Position";
    public static final String isOpenOrder = "isOpenOrder";

    public static final String position = "position";
    public static final String title = "title";

    public static final String name = "name";
    public static final String password = "password";
    public static final String phone = "phone";
    public static final String city = "city";
    public static final String email = "email";
    public static final String category = "category";
    public static final String subcategory = "subcategory";
    public static final String smscode = "smscode";
    public static final String photo = "photo";

    public static final String keyUser = "user";
    public static final String keyCategory = "category";
    public static final String keyPodcategory = "podcategory";
    public static final String keyDistrict = "district";
    public static final String keyCity = "city";

    public static final String keyCityUerSelected = "userSelectedCity";

    public static final String REGISTRATION_COMPLETE = "registrationComplite";


}
