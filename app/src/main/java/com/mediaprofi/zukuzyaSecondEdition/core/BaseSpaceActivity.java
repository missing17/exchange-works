package com.mediaprofi.zukuzyaSecondEdition.core;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.mediaprofy.zukuzyaSecondEdition.R;
import com.octo.android.robospice.JacksonGoogleHttpClientSpiceService;
import com.octo.android.robospice.JacksonSpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;

/**
 * Created by aleksei on 01.12.15.
 */
public class BaseSpaceActivity extends AppCompatActivity {

    private SpiceManager springManager = new SpiceManager(JacksonSpringAndroidSpiceService.class);

    private SpiceManager spiceManager = new SpiceManager(JacksonGoogleHttpClientSpiceService.class);
    private SharedPreferences sharedPreferences;
    protected Toolbar myToolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPreferences = getSharedPreferences(Constants.nameSharedPrefernces, MODE_PRIVATE);
    }

    @Override
    public void onStart(){
        spiceManager.start(this);
        springManager.start(this);
        super.onStart();
    }

    @Override
    public void onStop(){
        spiceManager.shouldStop();
        springManager.shouldStop();
        super.onStop();
    }

    public SpiceManager getSpiceManager(){
        return spiceManager;
    }

    public SpiceManager getSpringSpiceMAnager(){
        return springManager;
    }

    public SharedPreferences getGloabalSetting(){
        return sharedPreferences;
    }

    public void showToast(int message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void buildActionBar(int title, boolean isUp, boolean logo){
        myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            myToolbar.setTitleTextColor(getResources().getColor(R.color.white, getApplicationContext().getTheme()));
        } else {
            myToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        }

        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(isUp);
//            ab.setLogo(R.drawable.ic_menu);
//            ab.setDisplayUseLogoEnabled(logo);
            ab.setTitle(title);
        }
    }

}
