package com.mediaprofi.zukuzyaSecondEdition.ui.district.model;

import com.google.api.client.util.Key;

import java.util.List;

/**
 * Created by aleksei on 23.12.15.
 */
public class DistrictModel  {

    @Key
    private List<District> city;

    public List<District> getCity() {
        return city;
    }

    public void setCity(List<District> city) {
        this.city = city;
    }

    public static class District{
        @Key
        private int id;

        @Key
        private String name_city;

        @Key
        private Integer kol_zak;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName_city() {
            return name_city;
        }

        public void setName_city(String name_city) {
            this.name_city = name_city;
        }

        public Integer getKol_zak() {
            return kol_zak;
        }

        public void setKol_zak(Integer kol_zak) {
            this.kol_zak = kol_zak;
        }
    }

}
