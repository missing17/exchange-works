package com.mediaprofi.zukuzyaSecondEdition.ui.myorder;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.mediaprofi.zukuzyaSecondEdition.IClickOnMyOrder;
import com.mediaprofi.zukuzyaSecondEdition.ui.myOrderDescription.MyOrderDescription;
import com.mediaprofy.zukuzyaSecondEdition.R;
import com.mediaprofi.zukuzyaSecondEdition.core.BaseSpaceActivity;
import com.mediaprofi.zukuzyaSecondEdition.core.Constants;
import com.mediaprofi.zukuzyaSecondEdition.ui.myorder.decoration.MyOrderDecoration;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aleksei on 15.12.15.
 */
public class MyOrderActivity extends BaseSpaceActivity implements IClickOnMyOrder {

    private RecyclerView myRecyclerView;
    private LinearLayoutManager linearLayoutManager;
    private MyOrderAdapter myOrederAdapter;
    private List<MyOrderModel.MyOrder> myOrder;

    @Override
    public void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.my_order_list);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            myToolbar.setTitleTextColor(getResources().getColor(R.color.white, getApplicationContext().getTheme()));
        } else {
            myToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        }

        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setTitle(R.string.my_order);
            ab.setDisplayHomeAsUpEnabled(true);
        }

        myOrder = new ArrayList<>();
        linearLayoutManager = new LinearLayoutManager(this);

        myRecyclerView = (RecyclerView) findViewById(R.id.myRecyclerView);
        myRecyclerView.setLayoutManager(linearLayoutManager);
        myRecyclerView.addItemDecoration(new MyOrderDecoration());

        myOrederAdapter = new MyOrderAdapter(this, myOrder, this);
        myRecyclerView.setAdapter(myOrederAdapter);

    }

    @Override
    public void onStart() {
        super.onStart();
        String token = getGloabalSetting().getString(Constants.tokenInSharedPreferences, "");
        MyOrderRequest myOrderRequest = new MyOrderRequest(token);
        getSpiceManager().execute(myOrderRequest, new MyOrderRequestListener());
    }

    @Override
    public void clickMyOrder(int position, String title) {
        int id = Integer.valueOf(myOrder.get(position).getId_zakaz());
        Intent intentDescription = new Intent(MyOrderActivity.this, MyOrderDescription.class);
        intentDescription.putExtra(Constants.positionOrder, id);
        startActivity(intentDescription);
    }

    private class MyOrderRequestListener implements RequestListener<MyOrderModel> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {

        }

        @Override
        public void onRequestSuccess(MyOrderModel myOrderModels) {
            myOrder = myOrderModels.getMyOrder();
            myOrederAdapter.updateMyOreder(myOrderModels.getMyOrder());
            myOrederAdapter.notifyDataSetChanged();
        }
    }
}
