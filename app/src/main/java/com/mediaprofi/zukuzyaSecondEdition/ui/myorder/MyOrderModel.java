package com.mediaprofi.zukuzyaSecondEdition.ui.myorder;

import com.google.api.client.util.Key;

import java.util.List;

/**
 * Created by aleksei on 15.12.15.
 */
public class MyOrderModel {

    @Key
    private List<MyOrder> myOrder;

    public List<MyOrder> getMyOrder() {
        return myOrder;
    }

    public void setMyOrder(List<MyOrder> myOrder) {
        this.myOrder = myOrder;
    }

    public static class MyOrder {

        @Key
        public String id_zakaz;

        @Key
        private String podkat;

        @Key
        private String street;

        @Key
        private String nom_doma;

        @Key
        private String date;

        @Key
        private String time;

        @Key
        private String status;

        public String getId_zakaz() {
            return id_zakaz;
        }

        public void setId_zakaz(String id_zakaz) {
            this.id_zakaz = id_zakaz;
        }

        public String getPodkat() {
            return podkat;
        }

        public void setPodkat(String podkat) {
            this.podkat = podkat;
        }

        public String getStreet() {
            return street;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public String getNom_doma() {
            return nom_doma;
        }

        public void setNom_doma(String nom_doma) {
            this.nom_doma = nom_doma;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
