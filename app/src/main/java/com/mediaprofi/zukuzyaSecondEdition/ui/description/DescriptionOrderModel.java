package com.mediaprofi.zukuzyaSecondEdition.ui.description;

import com.google.api.client.util.Key;

/**
 * Created by aleksei on 28.12.15.
 */
public class DescriptionOrderModel {

    @Key
    private int id;

    @Key
    private String city;

    @Key
    private String rajon;

    @Key
    private String podkateg;

    @Key
    private String kateg;

    @Key
    private String opis;

    @Key
    private String street;

    @Key
    private String dom;

    @Key
    private String wnen;

    @Key
    private String time;

    @Key
    private String price;

    @Key
    private String phone;

    @Key
    private String name;

    @Key
    private String date_reg;

    @Key
    private String flag;

    @Key
    private String status;

    @Key
    private String foto1;

    @Key
    private String foto2;

    @Key
    private String foto3;

    @Key
    private String foto4;

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRajon() {
        return rajon;
    }

    public void setRajon(String rajon) {
        this.rajon = rajon;
    }

    public String getDate_reg() {
        return date_reg;
    }

    public void setDate_reg(String date_reg) {
        this.date_reg = date_reg;
    }

    public String getKateg() {
        return kateg;
    }

    public void setKateg(String kateg) {
        this.kateg = kateg;
    }

    public String getPodkateg() {
        return podkateg;
    }

    public void setPodkateg(String podkateg) {
        this.podkateg = podkateg;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getDom() {
        return dom;
    }

    public void setDom(String dom) {
        this.dom = dom;
    }

    public String getWnen() {
        return wnen;
    }

    public void setWnen(String wnen) {
        this.wnen = wnen;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFoto1() {
        return foto1;
    }

    public void setFoto1(String foto1) {
        this.foto1 = foto1;
    }

    public String getFoto2() {
        return foto2;
    }

    public void setFoto2(String foto2) {
        this.foto2 = foto2;
    }

    public String getFoto3() {
        return foto3;
    }

    public void setFoto3(String foto3) {
        this.foto3 = foto3;
    }

    public String getFoto4() {
        return foto4;
    }

    public void setFoto4(String foto4) {
        this.foto4 = foto4;
    }
}
