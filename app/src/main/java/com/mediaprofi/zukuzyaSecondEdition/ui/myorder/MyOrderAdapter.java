package com.mediaprofi.zukuzyaSecondEdition.ui.myorder;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mediaprofi.zukuzyaSecondEdition.IClickOnMyOrder;
import com.mediaprofy.zukuzyaSecondEdition.R;

import java.util.List;

/**
 * Created by aleksei on 15.12.15.
 */
public class MyOrderAdapter extends RecyclerView.Adapter<MyOrderAdapter.ViewHolder> {

    private List<MyOrderModel.MyOrder> myOrder;
    private IClickOnMyOrder clickOnMyOrder;
    private Context mContext;

    public MyOrderAdapter(Context context, List<MyOrderModel.MyOrder> myOrder, IClickOnMyOrder clickOnMyOrder) {
        this.mContext = context;
        this.myOrder = myOrder;
        this.clickOnMyOrder = clickOnMyOrder;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_order, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MyOrderModel.MyOrder orderModel = myOrder.get(position);

        holder.adress.setText(orderModel.getStreet() + " " + orderModel.getNom_doma());
        holder.subject.setText(orderModel.getPodkat());
        holder.date.setText(orderModel.getDate() + " " + orderModel.getTime());

        if (orderModel.getStatus().equals("Выполнено")) {
            holder.status.setText(orderModel.getStatus());
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                holder.status.setTextColor(mContext.getResources().getColor(R.color.txt_enter, mContext.getTheme()));
            } else {
                holder.status.setTextColor(mContext.getResources().getColor(R.color.txt_enter));
            }
        } else {
            holder.status.setText(orderModel.getStatus());
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                holder.status.setTextColor(mContext.getResources().getColor(R.color.border_btn, mContext.getTheme()));
            } else {
                holder.status.setTextColor(mContext.getResources().getColor(R.color.border_btn));
            }
        }
    }

    @Override
    public int getItemCount() {
        return myOrder.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView subject;
        public TextView adress;
        public TextView date;
        public TextView status;

        public ViewHolder(View itemView) {
            super(itemView);

            subject = (TextView) itemView.findViewById(R.id.subject);
            adress = (TextView) itemView.findViewById(R.id.adress);
            date = (TextView) itemView.findViewById(R.id.date);
            status = (TextView) itemView.findViewById(R.id.status);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickOnMyOrder != null){
                int i = getLayoutPosition();
                clickOnMyOrder.clickMyOrder(i, " ");
            }
        }
    }

    public void updateMyOreder(List<MyOrderModel.MyOrder> newOrder){
        this.myOrder = newOrder;
    }
}
