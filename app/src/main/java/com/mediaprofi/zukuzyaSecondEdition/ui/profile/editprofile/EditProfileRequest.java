package com.mediaprofi.zukuzyaSecondEdition.ui.profile.editprofile;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpContent;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.UrlEncodedContent;
import com.google.api.client.json.jackson.JacksonFactory;
import com.mediaprofi.zukuzyaSecondEdition.core.Constants;
import com.mediaprofi.zukuzyaSecondEdition.ui.settings.changepswd.ChangeModel;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by aleksei on 26.01.16.
 */
public class EditProfileRequest extends GoogleHttpClientSpiceRequest<ChangeModel> {

    HashMap<String, Object> postParam;
    String token = "";

    public EditProfileRequest(String name, String mail, String photo, String subCategory, String token) {
        super(ChangeModel.class);

        this.token = token;

        postParam = new HashMap<>();
        postParam.put("name", name);
        postParam.put("mail", mail);
        postParam.put("image", photo);
        postParam.put("podkat", subCategory);
    }

    @Override
    public ChangeModel loadDataFromNetwork() throws Exception {
        HttpRequest request = null;
        String url = Constants.baseURL + "/redatirovat.php";
        GenericUrl genericUrl = new GenericUrl(url);


        HttpContent content = new UrlEncodedContent(postParam);
        request = buildPostRequest(genericUrl, content);

        request.setParser(new JacksonFactory().createJsonObjectParser());

        HttpResponse response = request.execute();
        return response.parseAs(getResultType());
    }

    private HttpRequest buildPostRequest(GenericUrl genericUrl, HttpContent content) throws IOException {
        System.setProperty("http.keepAlive", "false");
        HttpRequest request = getHttpRequestFactory().buildPostRequest(genericUrl, content);
        customHttpHeader(request);
        return request;
    }

    private void customHttpHeader(HttpRequest request) {
        request.getHeaders().set("X-Token", token);
        request.getHeaders().setAcceptEncoding("gzip, deflate");
        request.getHeaders().set("Connection", "close");
        request.getHeaders().setAccept("text/html,application/xml,application/json");
    }
}
