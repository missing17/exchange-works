package com.mediaprofi.zukuzyaSecondEdition.ui.registartion.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mediaprofi.zukuzyaSecondEdition.MainActivity;
import com.mediaprofi.zukuzyaSecondEdition.core.BaseSpaceActivity;
import com.mediaprofi.zukuzyaSecondEdition.core.Constants;
import com.mediaprofi.zukuzyaSecondEdition.customView.CustomFiled;
import com.mediaprofi.zukuzyaSecondEdition.customView.UsPhoneNumberFormatter;
import com.mediaprofi.zukuzyaSecondEdition.model.TokenModel;
import com.mediaprofi.zukuzyaSecondEdition.requests.TokenRequest;
import com.mediaprofi.zukuzyaSecondEdition.ui.registartion.registration.RegistartionActivity;
import com.mediaprofy.zukuzyaSecondEdition.R;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.lang.ref.WeakReference;

public class LoginActivity extends BaseSpaceActivity {

    private SharedPreferences sharedPreferences;
    private TokenRequest tokenRequest;
    private RelativeLayout rlWaite;

    private CustomFiled cfLogin;
    private CustomFiled cfPassword;

    private Context mContext;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mContext = this;

        sharedPreferences = getSharedPreferences(Constants.nameSharedPrefernces, MODE_PRIVATE);

        String token = sharedPreferences.getString(Constants.tokenInSharedPreferences, "");

        if (token != null && token.length() != 0) {
            startMainActivity();
        } else {
            rlWaite = (RelativeLayout) findViewById(R.id.rlWaite);
            cfLogin = (CustomFiled) findViewById(R.id.login);
            cfPassword = (CustomFiled) findViewById(R.id.password);

            UsPhoneNumberFormatter addLineNumberFormatter = new UsPhoneNumberFormatter(
                    new WeakReference<>(cfLogin.getEditTextView()));
            cfLogin.getEditTextView().addTextChangedListener(addLineNumberFormatter);

            Button btnEnter = (Button) findViewById(R.id.btnEnter);
            btnEnter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (cfLogin.getEntreText().length() != 0 && cfPassword.getEntreText().length() != 0) {
                        comin("7" + UsPhoneNumberFormatter.deformatUsNumber(cfLogin.getEditTextView().getText()), cfPassword.getEntreText());
                    } else {
                        showToast(R.string.empty_fileds);
                    }
                }
            });

            //начать регистрацию
            TextView txtRegistration = (TextView) findViewById(R.id.txtRegistration);
            txtRegistration.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    rlWaite.setVisibility(View.VISIBLE);
                    startActivity(new Intent(LoginActivity.this, RegistartionActivity.class));
                }
            });
        }
    }

    public Context getContext(){
        return mContext;
    }

    private void comin(String login, String password) {
        rlWaite.setVisibility(View.VISIBLE);
        tokenRequest = new TokenRequest(login, password);
        getSpiceManager().execute(tokenRequest, new TokenRequestListener());
    }

    private void wrongAuthorization() {
        showToast(R.string.wrong_authorization);
    }

    private void goodAuthorization(String token) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Constants.tokenInSharedPreferences, token);
        editor.apply();

        startMainActivity();
    }

    private void startMainActivity() {
        Intent mainIntent = new Intent(LoginActivity.this, MainActivity.class);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(mainIntent);
        finish();
    }

    private class TokenRequestListener implements RequestListener<TokenModel> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            rlWaite.setVisibility(View.GONE);
            showToast(R.string.error_load_from_server);
        }

        @Override
        public void onRequestSuccess(TokenModel tokenModel) {
            rlWaite.setVisibility(View.GONE);
            if (!tokenModel.getToken().equals("error")) {
                goodAuthorization(tokenModel.getToken());
            } else {
                wrongAuthorization();
            }
        }
    }
}
