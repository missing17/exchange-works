package com.mediaprofi.zukuzyaSecondEdition.ui.confirm;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpContent;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.UrlEncodedContent;
import com.google.api.client.json.jackson.JacksonFactory;
import com.mediaprofi.zukuzyaSecondEdition.core.Constants;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by aleksei on 19.01.16.
 */
public class SendAllDataRequest extends GoogleHttpClientSpiceRequest<RegTokenModel>{

    private HashMap<String, String> parameters;
    public SendAllDataRequest(String name, String phone, String mail, String password,
                              String category, String subCatForRequest, String city, String photo) {
        super(RegTokenModel.class);

        parameters = new HashMap<>();
        parameters.put("phone_reg", phone);
        parameters.put("passwd", password);
        parameters.put("name", name);
        parameters.put("mail", mail);
        parameters.put("kat", category);
        parameters.put("podkat", subCatForRequest);
        parameters.put("city", city);
        parameters.put("reg","mobile");
        parameters.put("image", photo);

        setRetryPolicy(null);
    }

    @Override
    public RegTokenModel loadDataFromNetwork() throws Exception {
        HttpRequest request = null;
        String url = Constants.baseURL + "/registr.php";
        GenericUrl genericUrl = new GenericUrl(url);


        HttpContent content = new UrlEncodedContent(parameters);
        request = buildPostRequest(genericUrl, content);

        request.setParser(new JacksonFactory().createJsonObjectParser());

        HttpResponse response = request.execute();

        return response.parseAs(getResultType());
    }

    private HttpRequest buildPostRequest(GenericUrl genericUrl, HttpContent content) throws IOException {
        System.setProperty("http.keepAlive", "false");
        HttpRequest request = getHttpRequestFactory().buildPostRequest(genericUrl, content);
        customHttpHeader(request);
        return request;
    }

    private void customHttpHeader(HttpRequest request) {
        request.getHeaders().setAcceptEncoding("gzip, deflate");
        request.getHeaders().set("Connection", "close");
        request.getHeaders().setAccept("text/html,application/xml,application/json");
//        request.getHeaders().setAcceptEncoding("gzip, deflate");
//        request.getHeaders().set("Connection", "close");
//        request.getHeaders().setContentType("application/x-www-form-urlencoded");
    }
}
