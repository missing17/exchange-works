package com.mediaprofi.zukuzyaSecondEdition.ui.district.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mediaprofi.zukuzyaSecondEdition.IClickOnMyOrder;
import com.mediaprofy.zukuzyaSecondEdition.R;
import com.mediaprofi.zukuzyaSecondEdition.ui.district.model.DistrictModel;

import java.util.List;

/**
 * Created by aleksei on 17.12.15.
 */
public class DistrictOrderAdapter extends RecyclerView.Adapter<DistrictOrderAdapter.ViewHolder> {

    private List<DistrictModel.District> listOrder;
    private IClickOnMyOrder clickOnMyOrder;
    private Context mContext;

    public DistrictOrderAdapter(List<DistrictModel.District> listOrder, IClickOnMyOrder clickOnMyOrder, Context context) {
        this.listOrder = listOrder;
        this.clickOnMyOrder = clickOnMyOrder;
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_district_order, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        DistrictModel.District districtOreder = listOrder.get(position);
        holder.txtTitle.setText(districtOreder.getName_city());
        holder.txtCount.setText(String.valueOf(districtOreder.getKol_zak()));

        if (districtOreder.getKol_zak() != 0) {
            Drawable drEmpty = ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.order_style_empty, null);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                holder.rlWrapper.setBackground(drEmpty);
            }
        } else {
            Drawable drEmpty = ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.order_style, null);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                holder.rlWrapper.setBackground(drEmpty);
            }
        }

    }

    @Override
    public int getItemCount() {
        return listOrder.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView txtTitle;
        public TextView txtCount;
        public RelativeLayout rlWrapper;

        public ViewHolder(View itemView) {
            super(itemView);
            txtTitle = (TextView) itemView.findViewById(R.id.txtNameDistrict);
            txtCount = (TextView) itemView.findViewById(R.id.txtCount);
            rlWrapper = (RelativeLayout) itemView.findViewById(R.id.rlWrapper);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getLayoutPosition();
            DistrictModel.District districtOreder = listOrder.get(position);
            clickOnMyOrder.clickMyOrder(districtOreder.getId(), districtOreder.getName_city());
        }
    }

    public void updateDateAdapters(List<DistrictModel.District> newOrders) {
        this.listOrder = newOrders;
    }
}
