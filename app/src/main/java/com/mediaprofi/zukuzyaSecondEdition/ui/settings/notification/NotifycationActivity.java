package com.mediaprofi.zukuzyaSecondEdition.ui.settings.notification;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

import com.mediaprofy.zukuzyaSecondEdition.R;
import com.mediaprofi.zukuzyaSecondEdition.core.BaseSpaceActivity;
import com.mediaprofi.zukuzyaSecondEdition.core.Constants;

/**
 * Created by aleksei on 16.12.15.
 */
public class NotifycationActivity extends BaseSpaceActivity implements CompoundButton.OnCheckedChangeListener {

    private  SharedPreferences sharedPreferences;
    private ToggleButton toggleButton;
    private boolean tmpValue;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notifycation);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            myToolbar.setTitleTextColor(getResources().getColor(R.color.white, getApplicationContext().getTheme()));
        } else {
            myToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        }

        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setTitle(R.string.settingsUpNotifycation);


        sharedPreferences = getGloabalSetting();
        final boolean isNotify = sharedPreferences.getBoolean(Constants.notificationImSharedPreferences, true);
        tmpValue = isNotify;

        toggleButton = (ToggleButton) findViewById(R.id.toggleButton);
        toggleButton.setChecked(isNotify);

        toggleButton.setOnCheckedChangeListener(this);
    }

    @Override
    public void onPause(){
        super.onPause();
        SharedPreferences.Editor editorSharedPreferences = sharedPreferences.edit();
        editorSharedPreferences.putBoolean(Constants.notificationImSharedPreferences, tmpValue);
        editorSharedPreferences.apply();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        toggleButton.setChecked(isChecked);
        tmpValue = isChecked;
    }
}
