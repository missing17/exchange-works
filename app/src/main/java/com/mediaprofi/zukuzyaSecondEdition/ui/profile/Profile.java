package com.mediaprofi.zukuzyaSecondEdition.ui.profile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mediaprofy.zukuzyaSecondEdition.R;
import com.mediaprofi.zukuzyaSecondEdition.core.App;
import com.mediaprofi.zukuzyaSecondEdition.core.BaseSpaceActivity;
import com.mediaprofi.zukuzyaSecondEdition.core.Constants;
import com.mediaprofi.zukuzyaSecondEdition.model.UserModel;
import com.mediaprofi.zukuzyaSecondEdition.ui.profile.editprofile.EditProfileActivity;
import com.mediaprofi.zukuzyaSecondEdition.ui.registartion.login.LoginActivity;

/**
 * Created by aleksei on 20.12.15.
 */
public class Profile extends BaseSpaceActivity {

    private UserModel userModel;
    private TextView txtName, txtPhone, txtMail, txtCategory, txtSubCateg;
    private ImageView imgAvatar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);

        buildActionBar(R.string.profile, true, false);
        userModel = (UserModel) App.getFromMemoryCache(Constants.keyUser);

        imgAvatar = (ImageView) findViewById(R.id.imgAvatar);
        txtName = (TextView) findViewById(R.id.txtName);
        txtPhone = (TextView) findViewById(R.id.txtPhone);
        txtMail = (TextView) findViewById(R.id.txtMail);
        txtCategory = (TextView) findViewById(R.id.txtCategory);
        txtSubCateg = (TextView) findViewById(R.id.txtSubCategory);

        Button edit = (Button) findViewById(R.id.edit);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent editIntent = new Intent(Profile.this, EditProfileActivity.class);
                startActivity(editIntent);
            }
        });

        Button exit = (Button) findViewById(R.id.exit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearSharedPreferences();
                userModel = null;
                App.putInMemoryCache(Constants.keyUser, userModel);
                Intent exitIntent = new Intent(Profile.this, LoginActivity.class);
                startActivity(exitIntent);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        if (userModel != null) {
            txtName.setText(userModel.getName());
            txtPhone.setText(userModel.getPhone());
            txtMail.setText(userModel.getMail());
            txtCategory.setText(userModel.getCateg_name());
            txtSubCateg.setText(getSubCategoryString());
            Glide.with(this).load(Constants.baseURLShort + userModel.getPhoto()).into(imgAvatar);
        } else {
            finish();
        }
    }

    private String getSubCategoryString() {
        String[] listSubCategory = userModel.getCateg_pod_name();

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < listSubCategory.length; i++) {
            stringBuilder.append(listSubCategory[i]);
            if (i != (listSubCategory.length - 1))
                stringBuilder.append(", ");
        }

        return stringBuilder.toString();
    }

    private void clearSharedPreferences() {
        SharedPreferences sharedPreferences = getGloabalSetting();//getActivity().getSharedPreferences(Constants.nameSharedPrefernces, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Constants.tokenInSharedPreferences, "");
        editor.putBoolean(Constants.notificationImSharedPreferences, true);
        editor.putBoolean(Constants.sendTokenOnServerSharedPreferences, false);
        editor.apply();
    }
}
