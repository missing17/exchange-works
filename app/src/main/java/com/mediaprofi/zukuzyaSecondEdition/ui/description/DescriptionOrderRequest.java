package com.mediaprofi.zukuzyaSecondEdition.ui.description;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.json.jackson.JacksonFactory;
import com.mediaprofi.zukuzyaSecondEdition.core.Constants;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

/**
 * Created by aleksei on 29.12.15.
 */
public class DescriptionOrderRequest extends GoogleHttpClientSpiceRequest<DescriptionOrderModel> {

    private int idOrder;
    private int isOpen;
    private String token;

    public DescriptionOrderRequest( int idOrder, int isOpen, String token) {
        super(DescriptionOrderModel.class);
        this.isOpen = isOpen;
        this.idOrder = idOrder;
        this.token = token;

//        this.idOrder = 358;
//        this.isOpen = 0;

    }

    @Override
    public DescriptionOrderModel loadDataFromNetwork() throws Exception {
        String url = Constants.baseURL + "/data_json.php?zakaz=" + this.idOrder + "&status=" + this.isOpen;
        HttpRequest request = getHttpRequestFactory()
                .buildGetRequest(new GenericUrl(url))
                .setHeaders(new HttpHeaders()
                .set("X-Token", token));
        request.setParser(new JacksonFactory().createJsonObjectParser());
        HttpResponse response = request.execute();
        return response.parseAs(getResultType());
    }
}
