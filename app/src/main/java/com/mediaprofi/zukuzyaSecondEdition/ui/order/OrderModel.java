package com.mediaprofi.zukuzyaSecondEdition.ui.order;

import com.google.api.client.util.Key;

import java.util.List;

/**
 * Created by aleksei on 15.12.15.
 */
public class OrderModel {

    public OrderModel() {}

    @Key
    private List<Order> rajon;

    public List<Order> getRajon() {
        return rajon;
    }

    public void setRajon(List<Order> rajon) {
        this.rajon = rajon;
    }

    public static class Order {

        @Key
        private int id;

        @Key
        private String opis;

        @Key
        private String street;

        @Key
        private String dom;

        @Key
        private String wnen;

        @Key
        private String time;

        @Key
        private String price;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getOpis() {
            return opis;
        }

        public void setOpis(String opis) {
            this.opis = opis;
        }

        public String getStreet() {
            return street;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public String getDom() {
            return dom;
        }

        public void setDom(String dom) {
            this.dom = dom;
        }

        public String getWnen() {
            return wnen;
        }

        public void setWnen(String wnen) {
            this.wnen = wnen;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }
    }
}
