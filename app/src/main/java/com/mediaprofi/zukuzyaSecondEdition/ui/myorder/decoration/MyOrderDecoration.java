package com.mediaprofi.zukuzyaSecondEdition.ui.myorder.decoration;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by aleksei on 17.12.15.
 */
public class MyOrderDecoration extends RecyclerView.ItemDecoration  {

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state){
        outRect.bottom = 15;
        outRect.top = 15;
    }
}
