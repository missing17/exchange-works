package com.mediaprofi.zukuzyaSecondEdition.ui.confirm;

import com.google.api.client.util.Key;

/**
 * Created by aleksei on 20.01.16.
 */
public class RegTokenModel {
    @Key
    private String autoriz;

    @Key
    private String token;

    public String getAutoriz() {
        return autoriz;
    }

    public void setAutoriz(String autoriz) {
        this.autoriz = autoriz;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
