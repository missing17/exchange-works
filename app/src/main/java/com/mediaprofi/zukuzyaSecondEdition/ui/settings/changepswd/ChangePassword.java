package com.mediaprofi.zukuzyaSecondEdition.ui.settings.changepswd;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.mediaprofy.zukuzyaSecondEdition.R;
import com.mediaprofi.zukuzyaSecondEdition.core.BaseSpaceActivity;
import com.mediaprofi.zukuzyaSecondEdition.core.Constants;
import com.mediaprofi.zukuzyaSecondEdition.customView.CustomFiled;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

/**
 * Created by aleksei on 25.01.16.
 */
public class ChangePassword extends BaseSpaceActivity {

    private CustomFiled newPassword, confirmNewPassword;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chenge_password);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            myToolbar.setTitleTextColor(getResources().getColor(R.color.white, getApplicationContext().getTheme()));
        } else {
            myToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        }

        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setTitle(R.string.chenge_password);

        newPassword = (CustomFiled) findViewById(R.id.password);
        confirmNewPassword = (CustomFiled) findViewById(R.id.confirm_password);

        Button btnSave = (Button) findViewById(R.id.btnEnter);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (newPassword.getEntreText().equals(confirmNewPassword.getEntreText())){
                    if (newPassword.getEntreText().length() >= 6){
                        changePassword(newPassword.getEntreText());
                    } else {
                        showToast(R.string.short_password);
                    }
                } else {
                    showToast(R.string.password_do_not_match);
                }
            }
        });
    }

    private void changePassword(String entreText) {
        String token = getGloabalSetting().getString(Constants.tokenInSharedPreferences, "");
        ChangePswdRequest chengePasswordRequest = new ChangePswdRequest(entreText, token);
        getSpiceManager().execute(chengePasswordRequest, new ChangePasswordListener());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    private class ChangePasswordListener implements RequestListener<ChangeModel>{

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            showToast(R.string.change_password_bad);
        }

        @Override
        public void onRequestSuccess(ChangeModel changeModel) {
            if (changeModel.getRedakt().equals("ok")){
                showToast(R.string.change_password_ok);
            }

        }
    }
}
