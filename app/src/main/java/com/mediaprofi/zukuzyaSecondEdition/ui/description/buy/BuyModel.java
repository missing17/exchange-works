package com.mediaprofi.zukuzyaSecondEdition.ui.description.buy;

import com.google.api.client.util.Key;

/**
 * Created by aleksei on 10.02.16.
 */
public class BuyModel {

    @Key
    private String kupleno;

    public String getKupleno() {
        return kupleno;
    }

    public void setKupleno(String kupleno) {
        this.kupleno = kupleno;
    }
}
