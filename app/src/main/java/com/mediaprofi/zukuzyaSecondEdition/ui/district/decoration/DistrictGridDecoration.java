package com.mediaprofi.zukuzyaSecondEdition.ui.district.decoration;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by aleksei on 17.12.15.
 */
public class DistrictGridDecoration extends RecyclerView.ItemDecoration {

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state){
        outRect.left = 15;
        outRect.right = 15;
        outRect.top = 10;

    }
}
