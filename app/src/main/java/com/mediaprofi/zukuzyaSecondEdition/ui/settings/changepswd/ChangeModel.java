package com.mediaprofi.zukuzyaSecondEdition.ui.settings.changepswd;

import com.google.api.client.util.Key;

/**
 * Created by aleksei on 26.01.16.
 */
public class ChangeModel {
    @Key
    private String redakt;

    public String getRedakt() {
        return redakt;
    }

    public void setRedakt(String redakt) {
        this.redakt = redakt;
    }
}
