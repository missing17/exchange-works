package com.mediaprofi.zukuzyaSecondEdition.ui.settings.problem;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.mediaprofy.zukuzyaSecondEdition.R;
import com.mediaprofi.zukuzyaSecondEdition.core.BaseSpaceActivity;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

/**
 * Created by aleksei on 19.12.15.
 */
public class ProblemActivity extends BaseSpaceActivity {

    EditText edtTitle, edtBody;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.problem);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            myToolbar.setTitleTextColor(getResources().getColor(R.color.white, getApplicationContext().getTheme()));
        } else {
            myToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        }

        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setTitle(R.string.report_problem);

        edtTitle = (EditText) findViewById(R.id.edtTitle);
        edtBody = (EditText) findViewById(R.id.edtBody);

        Button sendProblem = (Button) findViewById(R.id.btnSend);
        sendProblem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendProblemOnServer();
            }
        });

    }

    private void sendProblemOnServer() {
        ProblemRequest problemRequest = new ProblemRequest(edtTitle.getText().toString(), edtBody.getText().toString());
        getSpiceManager().execute(problemRequest, new ProblemListener());
    }

    private class ProblemListener implements RequestListener<ProblemModel> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            Log.e("ProblemActi", "bad");
        }

        @Override
        public void onRequestSuccess(ProblemModel problemModel) {
            messageSendOk();
            Log.e("ProblemActi", "okd");
        }
    }

    private void messageSendOk() {
        showToast(R.string.problem_send);
        edtTitle.setText("");
        edtBody.setText("");
//        finish();
    }
}
