package com.mediaprofi.zukuzyaSecondEdition.ui.registartion.registration;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpContent;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.UrlEncodedContent;
import com.google.api.client.json.jackson.JacksonFactory;
import com.mediaprofi.zukuzyaSecondEdition.core.Constants;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by aleksei on 19.01.16.
 */
public class SMSCodeRequest extends GoogleHttpClientSpiceRequest<SMSCodeModel> {

    private String phone;
    HashMap<String, Object> postParam;

    public SMSCodeRequest(String phone) {
        super(SMSCodeModel.class);
        this.phone = phone;


        postParam = new HashMap<>();
        postParam.put("phone", phone);
    }

    @Override
    public SMSCodeModel loadDataFromNetwork() throws Exception {

        HttpRequest request = null;
        String url = Constants.baseURL + "/data_json.php";
        GenericUrl genericUrl = new GenericUrl(url);


        HttpContent content = new UrlEncodedContent(postParam);
        request = buildPostRequest(genericUrl, content);

        request.setParser(new JacksonFactory().createJsonObjectParser());

        HttpResponse response = request.execute();

//        String answer = IOUtils.toString(response.getContent());
//        Log.e("LoadFile", answer);

        return response.parseAs(getResultType());
    }

    private HttpRequest buildPostRequest(GenericUrl genericUrl, HttpContent content) throws IOException {
        System.setProperty("http.keepAlive", "false");
        HttpRequest request = getHttpRequestFactory().buildPostRequest(genericUrl, content);
        customHttpHeader(request);
        return request;
    }

    private void customHttpHeader(HttpRequest request) {
        request.getHeaders().setAcceptEncoding("gzip, deflate");
        request.getHeaders().set("Connection", "close");
        request.getHeaders().setAccept("text/html,application/xml,application/json");
    }
}
