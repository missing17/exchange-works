package com.mediaprofi.zukuzyaSecondEdition.ui.order;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mediaprofi.zukuzyaSecondEdition.IClickOnMyOrder;
import com.mediaprofy.zukuzyaSecondEdition.R;

import java.util.List;

/**
 * Created by aleksei on 15.12.15.
 */
public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {
    private List<OrderModel.Order> myOrder;
    private IClickOnMyOrder clickOnMyOrder;

    public OrderAdapter(List<OrderModel.Order> myOrder, IClickOnMyOrder clickOnMyOrder) {
        this.myOrder = myOrder;
        this.clickOnMyOrder = clickOnMyOrder;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        OrderModel.Order order = myOrder.get(position);

        holder.adress.setText(order.getStreet() + " д. " + order.getDom());
        holder.subject.setText(order.getOpis());
        holder.date.setText(order.getWnen());
        if (order.getTime().length() != 0)
            holder.date.append(" в " + order.getTime());
        if (order.getPrice().length() != 0)
            holder.price.setText(order.getPrice() + " руб.");
    }

    @Override
    public int getItemCount() {
        return myOrder.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView subject;
        public TextView adress;
        public TextView date;
        public TextView price;

        public ViewHolder(View itemView) {
            super(itemView);

            subject = (TextView) itemView.findViewById(R.id.subject);
            adress = (TextView) itemView.findViewById(R.id.adress);
            date = (TextView) itemView.findViewById(R.id.date);
            price = (TextView) itemView.findViewById(R.id.price);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickOnMyOrder != null){
                int i = getLayoutPosition();
                OrderModel.Order order = myOrder.get(i);
                clickOnMyOrder.clickMyOrder(order.getId(), " ");
            }
        }
    }
}
