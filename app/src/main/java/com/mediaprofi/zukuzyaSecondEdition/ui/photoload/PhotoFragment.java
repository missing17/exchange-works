package com.mediaprofi.zukuzyaSecondEdition.ui.photoload;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mediaprofy.zukuzyaSecondEdition.R;
import com.mediaprofi.zukuzyaSecondEdition.core.App;
import com.mediaprofi.zukuzyaSecondEdition.core.Constants;
import com.mediaprofi.zukuzyaSecondEdition.customView.ChoiseDialog;
import com.mediaprofi.zukuzyaSecondEdition.model.UserModel;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by aleksei on 16.01.16.
 */
public class PhotoFragment extends Fragment {

    private final int GALERY_IMG = 1;
    private final int CAMERA_IMG = 2;

    private File photoFile = null;
    private Typeface font;
    private String mCurrentPhotoPath;
    private ImageView imgAvatar;
    private TextView txtPlus, txtAddFoto;
    private ProgressBar pbWaiteLoadPhoto;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        font = Typeface.createFromAsset(getActivity().getAssets(), "fontawesome-webfont.ttf");

        View v = inflater.inflate(R.layout.button_add_photo, container, false);

        pbWaiteLoadPhoto = (ProgressBar) v.findViewById(R.id.pbWaitLoadPhoto);

        imgAvatar = (ImageView) v.findViewById(R.id.imgAvatar);

        txtAddFoto = (TextView) v.findViewById(R.id.txtAddFoto);

        txtPlus = (TextView) v.findViewById(R.id.txtPlus);
        txtPlus.setTypeface(font);

        RelativeLayout addFoto = (RelativeLayout) v.findViewById(R.id.addPhoto);
        addFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickLoadPhoto();
            }
        });

        UserModel userModel = (UserModel) App.getFromMemoryCache(Constants.keyUser);
        if (userModel != null && userModel.getPhoto().length() != 0){
            imgAvatar.setVisibility(View.VISIBLE);
            txtAddFoto.setVisibility(View.GONE);
            txtPlus.setVisibility(View.GONE);

            Glide.with(getActivity()).load(Constants.baseURLShort + userModel.getPhoto()).into(imgAvatar);
        }
        return v;
    }

    private void clickLoadPhoto() {
        ChoiseDialog dialog;
        ArrayList<String> list = new ArrayList<>();
        list.add("Сделать снимок");
        list.add("Загрузить с телефона");

        DialogInterface.OnClickListener clikc = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 1)
                    getPhotoFromGalery();
                else {
                    if (hasPermission("android.permission.WRITE_EXTERNAL_STORAGE")) {
                        getPhotoFromCamera();
                    } else {
                        askRequestForCamera();
                    }
                }
                dialog.dismiss();
            }
        };

        dialog = new ChoiseDialog();
        dialog.setList(list);
        dialog.setClick(clikc);
        dialog.show(getFragmentManager(), "Tag");
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void askRequestForCamera() {
        String[] permission = {"android.permission.WRITE_EXTERNAL_STORAGE"};
        int permsRequestCode = 200;
        requestPermissions(permission, permsRequestCode);
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean hasPermission(String permission){
        if(checkVersionOS()){
            return(getActivity().checkSelfPermission(permission)==PackageManager.PERMISSION_GRANTED);
        }
        return true;

    }

    private boolean shouldWeAsk(String permission){
        SharedPreferences sharedPreferences =
                getActivity().getSharedPreferences(Constants.nameSharedPrefernces, Context.MODE_PRIVATE);
        return (sharedPreferences.getBoolean(permission, true));
    }



    private void markAsAsked(String permission){
        SharedPreferences sharedPreferences =
                getActivity().getSharedPreferences(Constants.nameSharedPrefernces, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(permission, false).apply();
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults){
        switch(permsRequestCode){

            case 200:
                boolean externalWrite = grantResults[0]== PackageManager.PERMISSION_GRANTED;
                if (externalWrite){
                    getPhotoFromCamera();
                }
                break;
        }
    }

    private boolean checkVersionOS(){
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    private void getPhotoFromGalery() {
        Intent intentAddFoto = new Intent(Intent.ACTION_PICK);
        intentAddFoto.setType("image/*");
        startActivityForResult(intentAddFoto, GALERY_IMG);
    }

    private void getPhotoFromCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            try {
                photoFile = createImageFile();
                setmCurrentPhotoPath(photoFile.getAbsolutePath());
            } catch (IOException ex) {
                Log.e("PhotoFragment", ex.toString());
            }
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));

                startActivityForResult(takePictureIntent, CAMERA_IMG);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        handlePhoto(requestCode, imageReturnedIntent);
    }

    public File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        return image;
    }

    public void setmCurrentPhotoPath(String mCurrentPhotoPath) {
        this.mCurrentPhotoPath = mCurrentPhotoPath;
    }

    public String getmCurrentPhotoPath() {
        return mCurrentPhotoPath;
    }

    private void handlePhoto(int requestCode, Intent data) {
        if (requestCode == CAMERA_IMG) {
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            if (getmCurrentPhotoPath() != null) {
                File f = new File(getmCurrentPhotoPath());
                Uri contentUri = Uri.fromFile(f);
                mediaScanIntent.setData(contentUri);
                getActivity().sendBroadcast(mediaScanIntent);
            }
        } else if (null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String imgDecodableString = cursor.getString(columnIndex);
            photoFile = new File(imgDecodableString);
            cursor.close();
        }

        showAvatar();
        new UploadFileAsync().execute(photoFile);
    }

    private void hideAvatar() {
        imgAvatar.setVisibility(View.GONE);
        txtAddFoto.setVisibility(View.VISIBLE);
        txtPlus.setVisibility(View.VISIBLE);
        pbWaiteLoadPhoto.setVisibility(View.GONE);
    }

    private void showAvatar() {
        imgAvatar.setVisibility(View.VISIBLE);
        txtAddFoto.setVisibility(View.GONE);
        txtPlus.setVisibility(View.GONE);

        pbWaiteLoadPhoto.setVisibility(View.VISIBLE);

        Glide.with(getActivity()).load(photoFile).into(imgAvatar);
    }

    private class UploadFileAsync extends AsyncTask<File, Void, String> {

        @Override
        protected String doInBackground(File... params) {
            String link = "";
            ObjectMapper om = new ObjectMapper();

            om.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, true);
            om.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, false);
            om.configure(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS, true);
            om.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, true);
            om.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, false);

            try {
                String sourceFileUri = "/mnt/sdcard/abc.png";

                HttpURLConnection conn = null;
                DataOutputStream dos = null;
                String lineEnd = "\r\n";
                String twoHyphens = "--";
                String boundary = "*****";
                int bytesRead, bytesAvailable, bufferSize;
                byte[] buffer;
                int maxBufferSize = 1 * 1024 * 1024;
                File sourceFile = params[0];

                if (sourceFile.isFile()) {

                    try {
                        String upLoadServerUri = Constants.baseURL + "/file.php";

                        // open a URL connection to the Servlet
                        FileInputStream fileInputStream = new FileInputStream(sourceFile);
                        URL url = new URL(upLoadServerUri);

                        // Open a HTTP connection to the URL
                        conn = (HttpURLConnection) url.openConnection();
                        conn.setDoInput(true); // Allow Inputs
                        conn.setDoOutput(true); // Allow Outputs
                        conn.setUseCaches(false); // Don't use a Cached Copy
                        conn.setRequestMethod("POST");
                        conn.setRequestProperty("Connection", "Keep-Alive");
                        conn.setRequestProperty("ENCTYPE",
                                "multipart/form-data");
                        conn.setRequestProperty("Content-Type",
                                "multipart/form-data;boundary=" + boundary);
                        conn.setRequestProperty("file", sourceFileUri);

                        dos = new DataOutputStream(conn.getOutputStream());

                        dos.writeBytes(twoHyphens + boundary + lineEnd);
                        dos.writeBytes("Content-Disposition: form-data; name=\"file\";filename=\""
                                + sourceFileUri + "\"" + lineEnd);

                        dos.writeBytes(lineEnd);

                        // create a buffer of maximum size
                        bytesAvailable = fileInputStream.available();

                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        buffer = new byte[bufferSize];

                        // read file and write it into form...
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                        while (bytesRead > 0) {
                            dos.write(buffer, 0, bufferSize);
                            bytesAvailable = fileInputStream.available();
                            bufferSize = Math
                                    .min(bytesAvailable, maxBufferSize);
                            bytesRead = fileInputStream.read(buffer, 0,
                                    bufferSize);
                        }
                        dos.writeBytes(lineEnd);
                        dos.writeBytes(twoHyphens + boundary + twoHyphens
                                + lineEnd);

                        // Responses from the server (code and message)
                        int serverResponseCode = conn.getResponseCode();

                        if (serverResponseCode == 200) {

                            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                            StringBuilder buf = new StringBuilder();
                            String line = null;
                            while ((line = reader.readLine()) != null) {
                                buf.append(line);
                            }
                            String str = buf.toString().trim();

                            JsonNode node = om.readTree(str.substring(1, str.length()).trim());
                            link = node.get("photo_ssylka").asText();
                        }

                        fileInputStream.close();
                        dos.flush();
                        dos.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } // End else block
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return link;
        }

        @Override
        protected void onPostExecute(String result) {
            if (!result.equals("error")) {
                sendOnActivity(result);
                pbWaiteLoadPhoto.setVisibility(View.GONE);
            } else {
                hideAvatar();
            }
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }

    }

    private void sendOnActivity(String link) {
        IGetPhotoLink activity = (IGetPhotoLink) getActivity();
        activity.useLinkOnPhoto(link);
        Log.e("send", link);
    }
}
