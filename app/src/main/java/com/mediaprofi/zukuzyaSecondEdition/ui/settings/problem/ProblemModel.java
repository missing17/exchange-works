package com.mediaprofi.zukuzyaSecondEdition.ui.settings.problem;

import com.google.api.client.util.Key;

/**
 * Created by aleksei on 26.01.16.
 */
public class ProblemModel {

    @Key
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
