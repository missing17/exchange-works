package com.mediaprofi.zukuzyaSecondEdition.ui.settings.changepswd;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpContent;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.UrlEncodedContent;
import com.google.api.client.json.jackson.JacksonFactory;
import com.mediaprofi.zukuzyaSecondEdition.core.Constants;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by aleksei on 26.01.16.
 */
public class ChangePswdRequest extends GoogleHttpClientSpiceRequest<ChangeModel> {

    private String token;
    private HashMap<String, Object> postParam;

    public ChangePswdRequest(String newPassword, String token) {
        super(ChangeModel.class);

        this.token = token;
        postParam = new HashMap<>();
        postParam.put("paswd", newPassword);
    }

    @Override
    public ChangeModel loadDataFromNetwork() throws Exception {
        HttpRequest request = null;
        String url = Constants.baseURL + "/repaswd.php";
        GenericUrl genericUrl = new GenericUrl(url);


        HttpContent content = new UrlEncodedContent(postParam);
        request = buildPostRequest(genericUrl, content);

        request.setParser(new JacksonFactory().createJsonObjectParser());

        HttpResponse response = request.execute();
        return response.parseAs(getResultType());
    }

    private HttpRequest buildPostRequest(GenericUrl genericUrl, HttpContent content) throws IOException {
        System.setProperty("http.keepAlive", "false");
        HttpRequest request = getHttpRequestFactory().buildPostRequest(genericUrl, content);
        customHttpHeader(request);
        return request;
    }

    private void customHttpHeader(HttpRequest request) {
        request.getHeaders().setAcceptEncoding("gzip, deflate");
        request.getHeaders().set("X-Token", token);
        request.getHeaders().setAccept("text/html,application/xml,application/json");
    }
}
