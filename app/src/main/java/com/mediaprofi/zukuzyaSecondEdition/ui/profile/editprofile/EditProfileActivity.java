package com.mediaprofi.zukuzyaSecondEdition.ui.profile.editprofile;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.mediaprofy.zukuzyaSecondEdition.R;
import com.mediaprofi.zukuzyaSecondEdition.core.App;
import com.mediaprofi.zukuzyaSecondEdition.core.BaseSpaceActivity;
import com.mediaprofi.zukuzyaSecondEdition.core.Constants;
import com.mediaprofi.zukuzyaSecondEdition.customView.ChoiseDialogCategory;
import com.mediaprofi.zukuzyaSecondEdition.customView.CustomFiled;
import com.mediaprofi.zukuzyaSecondEdition.customView.CustomFiledSpinner;
import com.mediaprofi.zukuzyaSecondEdition.customView.MultyChoiseDialog;
import com.mediaprofi.zukuzyaSecondEdition.model.CategoryModel;
import com.mediaprofi.zukuzyaSecondEdition.model.PodcategoryModel;
import com.mediaprofi.zukuzyaSecondEdition.model.UserModel;
import com.mediaprofi.zukuzyaSecondEdition.requests.CategoryRequest;
import com.mediaprofi.zukuzyaSecondEdition.requests.PodcategoryRequest;
import com.mediaprofi.zukuzyaSecondEdition.ui.photoload.IGetPhotoLink;
import com.mediaprofi.zukuzyaSecondEdition.ui.registartion.registration.RegistartionActivity;
import com.mediaprofi.zukuzyaSecondEdition.ui.photoload.PhotoFragment;
import com.mediaprofi.zukuzyaSecondEdition.ui.settings.changepswd.ChangeModel;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aleksei on 20.12.15.
 */
public class EditProfileActivity extends BaseSpaceActivity implements IGetPhotoLink {

    private List<Integer> listIdChoise;
    private CustomFiledSpinner customCategory, subCategory;
    private ChoiseDialogCategory dialog;
    private UserModel userModel;
    private String photoLink;
    private List<String> subCategId;
    private List<String> subCategName;
    private Context mContext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile);

        buildActionBar(R.string.edit_profile, true, false);
        subCategId = new ArrayList<>();
        subCategName = new ArrayList<>();
        mContext = this;

        userModel = (UserModel) App.getFromMemoryCache(Constants.keyUser);
        String category = userModel.getCateg_id();
        if (category.length() != 0) {
            goOnSreverByPodcategory(category);
        }

        final CustomFiled customName = (CustomFiled) findViewById(R.id.name);
        customName.setEntreText(userModel.getName());

        final CustomFiled customMail = (CustomFiled) findViewById(R.id.email);
        customMail.setEntreText(userModel.getMail());

        customCategory = (CustomFiledSpinner) findViewById(R.id.category);
        customCategory.setTextOnTextView(userModel.getCateg_name());

        String[] listSubCategory = userModel.getCateg_pod_name();

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < listSubCategory.length; i++) {
            stringBuilder.append(listSubCategory[i]);
            if (i != (listSubCategory.length - 1))
                stringBuilder.append(", ");
        }

        subCategory = (CustomFiledSpinner) findViewById(R.id.subcategory);
        subCategory.setTextOnTextView(stringBuilder.toString());

        checkNeededData();

        Fragment photoFragment = new PhotoFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(R.id.photoFragment, photoFragment);
        ft.commit();

        Button btnSave = (Button) findViewById(R.id.btnEnter);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveOnServer(customName.getEntreText(), customMail.getEntreText());
            }
        });
    }

    private void saveOnServer(String name, String mail) {
        StringBuilder strBuilder = new StringBuilder();
        for (int i = 0; i < subCategId.size(); i++) {
            strBuilder.append(subCategId.get(i));
            if (i != (subCategId.size() - 1))
                strBuilder.append(" ,");
        }

        String token = getGloabalSetting().getString(Constants.tokenInSharedPreferences, "");
        EditProfileRequest editProfileRequest = new EditProfileRequest(name, mail, photoLink, strBuilder.toString(), token);
        getSpiceManager().execute(editProfileRequest, new EditProfileSendListener(name, mail, photoLink, strBuilder.toString()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    private void checkNeededData() {
        List<CategoryModel.Category> listCategory =
                (List<CategoryModel.Category>) App.getFromMemoryCache(Constants.keyCategory);

        if (listCategory == null || listCategory.size() == 0) {
            goOnServerByCategory();
        } else {
            buildCategoruField(listCategory);
        }
    }

    private void goOnServerByCategory() {
        CategoryRequest categoryRequest = new CategoryRequest();
        getSpiceManager().execute(categoryRequest, new CategoryRequestListener());
    }

    private void selectCategory(DialogInterface dialog, int which, List<CategoryModel.Category> newlistCategory) {
        customCategory.setTextOnTextView(newlistCategory.get(which).getName_kategor());
        String categoryId = newlistCategory.get(which).getId();
        dialog.dismiss();

        userModel.setCateg_id(categoryId);
        userModel.setCateg_name(newlistCategory.get(which).getName_kategor());

        goOnSreverByPodcategory(categoryId);
    }

    @Override
    public void useLinkOnPhoto(String link) {
        photoLink = link;
    }

    private class MultiChoisShowClickListener implements View.OnClickListener {
        private List<PodcategoryModel.Podcategory> listPodcategory;

        public MultiChoisShowClickListener(List<PodcategoryModel.Podcategory> listPodcategory) {
            this.listPodcategory = listPodcategory;
        }

        @Override
        public void onClick(View v) {
            if (listPodcategory != null && listPodcategory.size() != 0) {
                MultyChoiseDialog dialog = new MultyChoiseDialog();
                dialog.setList(listPodcategory);
                dialog.setClick(new PodacategoryChoise(listPodcategory));
                dialog.show(getFragmentManager(), "Tag");
            }
        }
    }

    private class PodacategoryChoise implements RegistartionActivity.MultiChoiseCallBack {
        private List<PodcategoryModel.Podcategory> listPodcategory;

        public PodacategoryChoise(List<PodcategoryModel.Podcategory> listPodcategory){
            this.listPodcategory = listPodcategory;
        }

        @Override
        public void getChoiseItem(List<Integer> lisyIdChoise) {
            listIdChoise = lisyIdChoise;
            updateSubcategoryFields(lisyIdChoise, listPodcategory);
        }
    }


    private void updateSubcategoryFields(List<Integer> lisyIdChoise, List<PodcategoryModel.Podcategory> listPodcategory) {

        for (int i = 0; i < lisyIdChoise.size(); i++) {
            int id = lisyIdChoise.get(i);
            subCategId.add(listPodcategory.get(id).getId());
            subCategName.add(listPodcategory.get(id).getName_podkategor());
        }

        StringBuilder strBuilder = new StringBuilder();
        for (int i = 0; i < subCategName.size(); i++) {
            strBuilder.append(subCategName.get(i));
            if (i != (subCategName.size() - 1))
                strBuilder.append(" ,");
        }

        subCategory.setTextOnTextView(strBuilder.toString());
    }

    private void goOnSreverByPodcategory(String categoryId) {
        PodcategoryRequest podcategoryRequest = new PodcategoryRequest(categoryId);
        getSpiceManager().execute(podcategoryRequest, new PodcategoryRequestListener());
    }

    private void buildCategoruField(final List<CategoryModel.Category> newlistCategory) {
        customCategory.setOnClickListener(new View.OnClickListener() {

            DialogInterface.OnClickListener clikc = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    selectCategory(dialog, which, newlistCategory);
                }
            };

            @Override
            public void onClick(View v) {
                dialog = new ChoiseDialogCategory();
                dialog.setList(newlistCategory);
                dialog.setClick(clikc);
                dialog.show(getFragmentManager(), "Tag");
            }
        });
    }

    private class CategoryRequestListener implements RequestListener<CategoryModel> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
//            endNetworkRequest(false, getString(R.string.no_data));
        }

        @Override
        public void onRequestSuccess(CategoryModel categoryModel) {
            App.putInMemoryCache(Constants.keyCategory, categoryModel.getKategor());
            List<CategoryModel.Category> listCategory = categoryModel.getKategor();
            if (listCategory != null && listCategory.size() != 0)
                buildCategoruField(listCategory);
        }
    }

    private class PodcategoryRequestListener implements RequestListener<PodcategoryModel> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            showToast(R.string.cant_load_subCategory);
        }

        @Override
        public void onRequestSuccess(PodcategoryModel podcategoryModel) {
            MultiChoisShowClickListener multiChoiseClickListener = new MultiChoisShowClickListener(podcategoryModel.getSubCategory());
            subCategory.setOnClickListener(multiChoiseClickListener);
        }
    }

    private class EditProfileSendListener implements RequestListener<ChangeModel> {
        String name, mail, photo, subCategory;

        public EditProfileSendListener(String name, String mail, String photo, String subCategory) {
            this.name = name;
            this.mail = mail;
            this.photo = photo;
            this.subCategory = subCategory;
        }

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            showToast(R.string.error_load_to_server);
        }

        @Override
        public void onRequestSuccess(ChangeModel changeModel) {
            if (changeModel.getRedakt().equals("ok")) {
                saveInMemory(name, mail, photo);
                finish();
            } else {
                Toast.makeText(mContext, changeModel.getRedakt(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void saveInMemory(String name, String mail, String photo) {
        userModel.setName(name);
        userModel.setMail(mail);

        if (photo != null && photo.length() != 0)
            userModel.setPhoto(photo);

        String[] subCatId = new String[subCategId.size()];
        subCatId = subCategId.toArray(subCatId);
        if (subCatId.length != 0)
            userModel.setCateg_pod_id(subCatId);

        String[] subCatName = new String[subCategName.size()];
        subCatName = subCategName.toArray(subCatName);
        if (subCatName.length != 0)
            userModel.setCateg_pod_name(subCatName);

        App.putInMemoryCache(Constants.keyUser, userModel);
    }
}
