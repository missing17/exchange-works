package com.mediaprofi.zukuzyaSecondEdition.ui.registartion.registration;

import com.google.api.client.util.Key;

/**
 * Created by aleksei on 19.01.16.
 */
public class SMSCodeModel {

    @Key
    private Integer kode;

    public Integer getKode() {
        return kode;
    }

    public void setKode(Integer kode) {
        this.kode = kode;
    }
}
