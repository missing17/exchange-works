package com.mediaprofi.zukuzyaSecondEdition.ui.myOrderDescription;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.json.jackson.JacksonFactory;
import com.mediaprofi.zukuzyaSecondEdition.core.Constants;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

/**
 * Created by aleksei on 29.12.15.
 */
public class MyOrderDescriptionRequest extends GoogleHttpClientSpiceRequest<MyOrderDescriptionModel> {

    private int idOrder;
    private String token;

    public MyOrderDescriptionRequest(int idOrder, String token) {
        super(MyOrderDescriptionModel.class);
        this.idOrder = idOrder;
        this.token = token;
    }

    @Override
    public MyOrderDescriptionModel loadDataFromNetwork() throws Exception {
        String url = Constants.baseURL + "/data_json.php?zakaz=" + this.idOrder + "&status=1";
        HttpRequest request = getHttpRequestFactory()
                .buildGetRequest(new GenericUrl(url))
                .setHeaders(new HttpHeaders()
                .set("X-Token", token));
        request.setParser(new JacksonFactory().createJsonObjectParser());
        HttpResponse response = request.execute();
        return response.parseAs(getResultType());
    }
}
