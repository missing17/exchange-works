package com.mediaprofi.zukuzyaSecondEdition.ui.photoload;

/**
 * Created by aleksei on 28.01.16.
 */
public interface IGetPhotoLink {

    void useLinkOnPhoto(String link);
}
