package com.mediaprofi.zukuzyaSecondEdition.ui.description;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.vending.billing.IInAppBillingService;
import com.bumptech.glide.Glide;
import com.mediaprofi.zukuzyaSecondEdition.core.App;
import com.mediaprofi.zukuzyaSecondEdition.model.UserModel;
import com.mediaprofy.zukuzyaSecondEdition.R;
import com.mediaprofi.zukuzyaSecondEdition.core.BaseSpaceActivity;
import com.mediaprofi.zukuzyaSecondEdition.core.Constants;
import com.mediaprofi.zukuzyaSecondEdition.customView.ChoiseDialog;
import com.mediaprofi.zukuzyaSecondEdition.ui.description.buy.BuyModel;
import com.mediaprofi.zukuzyaSecondEdition.ui.description.buy.BuyRequest;
import com.mediaprofi.zukuzyaSecondEdition.ui.description.buy.BuySubscriteRequest;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by aleksei on 16.12.15.
 */
public class DescriptionOrder extends BaseSpaceActivity {

    private final int VERSION_BILLING = 3;
    private Context mContext;
    private Button btnGetOrder;
    //    private DescriptionOrderRequest descriptionRequest;
    private RelativeLayout rlWaite;
    private RelativeLayout rlNoData;
    private TextView txtNotyfication;
    private ScrollView myScrollView;
    private Bundle bundle;
    private String phoneNumber = "";

    private boolean userHaveSubs = false;
    private boolean openPhoneNumber = false;
    private IInAppBillingService mService;

    private TextView subject;
    private TextView price;
    private TextView categoty, city, adress, area;
    private TextView fullDescription, nameCustomer, phoneCustomer;
    private TextView datePresent, warried;
    private int idProduct;
    private boolean isOpenOrder;
    private int idOrder;
    private String token;
    private ImageView foto1, foto2, foto3, foto4;
    private UserModel userModel;

    @Override
    public void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.desc_order);
        userModel = (UserModel) App.getFromMemoryCache(Constants.keyUser);
        mContext = this;
        Intent serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        serviceIntent.setPackage("com.android.vending");
        bindService(serviceIntent, mServiceConnection, Context.BIND_AUTO_CREATE);

        subject = (TextView) findViewById(R.id.subject);
        price = (TextView) findViewById(R.id.price);
        categoty = (TextView) findViewById(R.id.category);
        city = (TextView) findViewById(R.id.city);
        adress = (TextView) findViewById(R.id.adress);
        area = (TextView) findViewById(R.id.area);
        fullDescription = (TextView) findViewById(R.id.fullDescription);
        nameCustomer = (TextView) findViewById(R.id.nameCustomer);
        phoneCustomer = (TextView) findViewById(R.id.phoneNumber);
        datePresent = (TextView) findViewById(R.id.datePresent);
        warried = (TextView) findViewById(R.id.warred);
        foto1 = (ImageView) findViewById(R.id.foto1);
        foto2 = (ImageView) findViewById(R.id.foto2);
        foto3 = (ImageView) findViewById(R.id.foto3);
        foto4 = (ImageView) findViewById(R.id.foto4);

        rlWaite = (RelativeLayout) findViewById(R.id.rlWaite);
        rlWaite.setVisibility(View.VISIBLE);

        rlNoData = (RelativeLayout) findViewById(R.id.rlNoData);
        txtNotyfication = (TextView) findViewById(R.id.txtNotyfication);

        myScrollView = (ScrollView) findViewById(R.id.myScrollView);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            myToolbar.setTitleTextColor(getResources().getColor(R.color.white, getApplicationContext().getTheme()));
        } else {
            myToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        }

        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setTitle("");

        bundle = getIntent().getExtras();
//        isOpenOrder = bundle.getBoolean(Constants.isOpenOrder);
        idOrder = bundle.getInt(Constants.positionOrder);

        SharedPreferences sharedPreferences = getSharedPreferences(Constants.nameSharedPrefernces, MODE_PRIVATE);
        token = sharedPreferences.getString(Constants.tokenInSharedPreferences, "");

        btnGetOrder = (Button) findViewById(R.id.btnGetOrder);
        btnGetOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userHaveSubs && !openPhoneNumber) {
                    openPhoneNumber = true;
                    phoneCustomer.setText(phoneNumber);
                } else {
                    if (userModel.getEst_zakaz().equals(0)) {
                        if (!isOpenOrder) {
                            shoseBuyMethod();
                        } else {
                            getForMyOrder();
                        }
                    } else {
                        showLimitError();
                    }
                }
            }
        });


        btnGetOrder.setVisibility(View.GONE);
        btnGetOrder.setVisibility(View.VISIBLE);
    }

    private void showLimitError() {
        Toast.makeText(this, R.string.limit_zakaz, Toast.LENGTH_LONG).show();
    }

    private void getForMyOrder() {
        BuyRequest buyRequest = new BuyRequest(token, String.valueOf(idProduct));
        getSpiceManager().execute(buyRequest, new BuyProductRequestListener());

        Toast.makeText(this, "Вы взяли заказ. Заказ перемещен в раздел мои заказы", Toast.LENGTH_SHORT).show();
    }


    private void shoseBuyMethod() {

        ChoiseDialog dialog;
        ArrayList<String> list = new ArrayList<>();
        list.add("Купить единоразово");
        list.add("Оформить подписку");

        DialogInterface.OnClickListener clikc = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    try {
                        buyOrederOnce();
                    } catch (IntentSender.SendIntentException e) {
                        e.printStackTrace();
                    }
                } else {
                    buySubscribe();
                }
                dialog.dismiss();
            }
        };

        dialog = new ChoiseDialog();
        dialog.setList(list);
        dialog.setClick(clikc);
        dialog.show(getFragmentManager(), "Tag");
    }

    private void buySubscribe() {
        GetInfoAboutProductSubscribe getSubscibe = new GetInfoAboutProductSubscribe();
        getSubscibe.execute();
    }

    private void buyOrederOnce() throws IntentSender.SendIntentException {
        GetInfoAboutProductOnce getInfoAboutProductOnce = new GetInfoAboutProductOnce();
        getInfoAboutProductOnce.execute();
    }

    @Override
    public void onStart() {
        super.onStart();
        DescriptionOrderRequest descriptionRequest = new DescriptionOrderRequest(idOrder, isOpenOrder ? 1 : 0, token);
        getSpiceManager().execute(descriptionRequest, new DescriptionListener());
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        bundle = intent.getBundleExtra("Info");
    }

    ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = IInAppBillingService.Stub.asInterface(service);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }
    };

    //    {"packageName":"com.OshurkovAlekseiDevelopment.squirrlsfree",
// "orderId":"transactionId.android.test.purchased",
// "productId":"android.test.purchased",
// "developerPayload":"52270",
// "purchaseTime":0,"purchaseState":0,
// "purchaseToken":"inapp:com.OshurkovAlekseiDevelopment.squirrlsfree:android.test.purchased"}
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1001) {
            int responseCode = data.getIntExtra("RESPONSE_CODE", 0);
            String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
            String dataSignature = data.getStringExtra("INAPP_DATA_SIGNATURE");

            if (resultCode == RESULT_OK) {
                isOpenOrder = true;
                try {
                    JSONObject object = new JSONObject(purchaseData);
                    String purshceToken = object.getString("purchaseToken");
                    int code = mService.consumePurchase(VERSION_BILLING, getPackageName(), purshceToken);
                    if (code == 0) {
                        BuyRequest buyRequest = new BuyRequest(token, object.getString("developerPayload"));
                        getSpiceManager().execute(buyRequest, new BuyProductRequestListener());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }

                DescriptionOrderRequest descriptionRequest = new DescriptionOrderRequest(idOrder, 1, token);
                getSpiceManager().execute(descriptionRequest, new DescriptionListener());
            }
        } else if (requestCode == 1003) {
            int responseCode = data.getIntExtra("RESPONSE_CODE", 0);
            String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
            String dataSignature = data.getStringExtra("INAPP_DATA_SIGNATURE");

            if (resultCode == RESULT_OK) {
                String prodId = "full_desc_week";
                isOpenOrder = true;
                try {
                    JSONObject object = new JSONObject(purchaseData);
                    String purshceToken = object.getString("purchaseToken");
                    prodId = object.getString("productId");

                    int code = mService.consumePurchase(VERSION_BILLING, getPackageName(), purshceToken);
                    if (code == 0) {
                        BuyRequest buyRequest = new BuyRequest(token, object.getString("developerPayload"));
                        getSpiceManager().execute(buyRequest, new BuyProductRequestListener());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }

                String sCodeId = "0";
                if (prodId.equals("full_desc_week")) {
                    sCodeId = "1";
                } else if (prodId.equals("full_desc_month")) {
                    sCodeId = "2";
                } else if (prodId.equals("full_desc_year")) {
                    sCodeId = "3";
                }

                BuySubscriteRequest subscribeRequest = new BuySubscriteRequest(token, sCodeId);
                getSpiceManager().execute(subscribeRequest, new SubscribeListener());

                DescriptionOrderRequest descriptionRequest = new DescriptionOrderRequest(idOrder, 1, token);
                getSpiceManager().execute(descriptionRequest, new DescriptionListener());
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mService != null) {
            unbindService(mServiceConnection);
        }
    }

    private void fullingLayout(DescriptionOrderModel descriptionModel) {

        idProduct = descriptionModel.getId();
        categoty.setText(descriptionModel.getKateg());
        if (descriptionModel.getPrice().length() != 0)
            price.setText(descriptionModel.getPrice() + " руб.");

        subject.setText(descriptionModel.getPodkateg());
        city.setText(descriptionModel.getCity());
        area.setText(descriptionModel.getRajon());

        adress.setText(descriptionModel.getStreet());

        if (descriptionModel.getDom().length() != 0)
            adress.append(" д. " + descriptionModel.getDom());

        fullDescription.setText(descriptionModel.getOpis());
        nameCustomer.setText(descriptionModel.getName());
        if (userModel.getDate_podpis().equals(""))
            phoneCustomer.setText(descriptionModel.getPhone()); // здесь
        else {
            userHaveSubs = true;
            descriptionModel.getPhone().substring(0, 3).concat("** ** ***");
        }

        if (descriptionModel.getFlag().equals("1")) {
//            isOpenOrder = true;
            btnGetOrder.setVisibility(View.GONE);
            warried.setVisibility(View.GONE);
        } else {
            btnGetOrder.setVisibility(View.VISIBLE);
            warried.setVisibility(View.VISIBLE);
        }

        datePresent.setText(descriptionModel.getDate_reg());

        btnGetOrder.setVisibility(View.VISIBLE);

        if (descriptionModel.getFoto1() != null && descriptionModel.getFoto1().length() != 0) {
            foto1.setVisibility(View.VISIBLE);
            Glide.with(this).load(descriptionModel.getFoto1()).into(foto1);
        }

        if (descriptionModel.getFoto2() != null && descriptionModel.getFoto2().length() != 0) {
            foto2.setVisibility(View.VISIBLE);
            Glide.with(this).load(descriptionModel.getFoto2()).into(foto2);
        }

        if (descriptionModel.getFoto3() != null && descriptionModel.getFoto3().length() != 0) {
            foto3.setVisibility(View.VISIBLE);
            Glide.with(this).load(descriptionModel.getFoto3()).into(foto3);
        }

        if (descriptionModel.getFoto4() != null && descriptionModel.getFoto4().length() != 0) {
            foto4.setVisibility(View.VISIBLE);
            Glide.with(this).load(descriptionModel.getFoto4()).into(foto4);
        }
    }


    private void endNetworkRequest(boolean sucess, String text) {
        if (sucess) {
            rlNoData.setVisibility(View.GONE);
            rlWaite.setVisibility(View.GONE);
            myScrollView.setVisibility(View.VISIBLE);
        } else {
            rlNoData.setVisibility(View.VISIBLE);
            rlWaite.setVisibility(View.GONE);
            myScrollView.setVisibility(View.GONE);
            txtNotyfication.setText(text);
        }
    }


    private class DescriptionListener implements RequestListener<DescriptionOrderModel> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            endNetworkRequest(false, getString(R.string.no_data));
        }

        @Override
        public void onRequestSuccess(DescriptionOrderModel descriptionOrderModel) {
            endNetworkRequest(true, null);
            phoneNumber = descriptionOrderModel.getPhone();
            fullingLayout(descriptionOrderModel);
        }
    }

    private class SubscribeListener implements RequestListener<BuyModel> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
        }

        @Override
        public void onRequestSuccess(BuyModel descriptionOrderModel) {
        }
    }

    private class BuyProductRequestListener implements RequestListener<BuyModel> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {

        }

        @Override
        public void onRequestSuccess(BuyModel buyModel) {
            userModel.setEst_zakaz(userModel.getEst_zakaz() + 1);
            App.putInMemoryCache(Constants.keyUser, userModel);
        }
    }

    //    ---------  Класс покупки  товара
    private class GetInfoAboutProductOnce extends AsyncTask<Void, Void, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONObject doInBackground(Void... params) {
            ArrayList<String> skuList = new ArrayList<>();
            skuList.add("full_description_once");
            JSONObject object = null;

            Bundle querySkus = new Bundle();
            querySkus.putStringArrayList("ITEM_ID_LIST", skuList);
            Bundle skuDetails;
            try {
                skuDetails = mService.getSkuDetails(VERSION_BILLING, getPackageName(), "inapp", querySkus);

                int response = skuDetails.getInt("RESPONSE_CODE");
                if (response == 0) {
                    ArrayList<String> responseList = skuDetails.getStringArrayList("DETAILS_LIST");
                    for (String thisResponse : responseList) {
                        try {
                            object = new JSONObject(thisResponse);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            return object;
        }

        @Override
        protected void onPostExecute(final JSONObject result) {
            super.onPostExecute(result);

            if (result != null) {
                try {

                    final String id = result.getString("productId");

                    final MaterialDialog mMaterialDialog = new MaterialDialog(mContext);
                    mMaterialDialog
                            .setTitle("Купить " + result.getString("title"))
                            .setMessage(result.getString("description") + " Цена : " + result.getString("price"))
                            .setPositiveButton("Купить", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try {
//                                        Bundle buyIntentBundle = mService.getBuyIntent(VERSION_BILLING, getPackageName(),
//                                                "android.test.purchased", "inapp", String.valueOf(idProduct));

                                        Bundle buyIntentBundle = mService.getBuyIntent(VERSION_BILLING, getPackageName(),
                                                id, "inapp", String.valueOf(idProduct));

                                        if (buyIntentBundle.getInt("RESPONSE_CODE") == 0) {
                                            PendingIntent pendingIntent = buyIntentBundle.getParcelable("BUY_INTENT");
                                            if (pendingIntent != null) {
                                                startIntentSenderForResult(pendingIntent.getIntentSender(),
                                                        1001, new Intent(), Integer.valueOf(0), Integer.valueOf(0),
                                                        Integer.valueOf(0));
                                            }
                                        }
                                    } catch (IntentSender.SendIntentException e) {
                                        e.printStackTrace();
                                    } catch (RemoteException e) {
                                        e.printStackTrace();
                                    }
                                    mMaterialDialog.dismiss();


                                }
                            })
                            .setNegativeButton("Отклонить", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mMaterialDialog.dismiss();
                                }
                            });

                    mMaterialDialog.show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class GetInfoAboutProductSubscribe extends AsyncTask<Void, Void, List<JSONObject>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected List<JSONObject> doInBackground(Void... params) {
            ArrayList<String> skuList = new ArrayList<>();
            skuList.add("subscribe_week");
            skuList.add("subscribe_month");
//            skuList.add("subscribe_year");

            List<JSONObject> listObject = new ArrayList<>();

            Bundle querySkus = new Bundle();
            querySkus.putStringArrayList("ITEM_ID_LIST", skuList);
            Bundle skuDetails;
            try {
                skuDetails = mService.getSkuDetails(VERSION_BILLING, getPackageName(), "subs", querySkus);

                int response = skuDetails.getInt("RESPONSE_CODE");
                if (response == 0) {
                    ArrayList<String> responseList = skuDetails.getStringArrayList("DETAILS_LIST");
                    for (String thisResponse : responseList) {
                        try {
                            listObject.add(new JSONObject(thisResponse));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }

            return listObject;
        }

        @Override
        protected void onPostExecute(final List<JSONObject> result) {
            super.onPostExecute(result);

            if (result != null) {
                try {
                    final ArrayList<String> product = new ArrayList<>();
                    for (JSONObject object : result) {
                        product.add(object.getString("title"));
                    }

                    ChoiseDialog dialog = new ChoiseDialog();
                    DialogInterface.OnClickListener clikc = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            JSONObject object = result.get(which);
                            buySubscribe(object);
                            dialog.dismiss();
                        }
                    };

                    dialog.setList(product);
                    dialog.setClick(clikc);
                    dialog.show(getFragmentManager(), "Tag");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    private void buySubscribe(JSONObject result) {

        try {
            final String id = result.getString("productId");

            final MaterialDialog mMaterialDialog = new MaterialDialog(mContext);
            mMaterialDialog
                    .setTitle("Купить " + result.getString("title"))
                    .setMessage(result.getString("description") + " Цена : " + result.getString("price"))
                    .setPositiveButton("Купить", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                Bundle buyIntentBundle = mService.getBuyIntent(VERSION_BILLING, getPackageName(),
                                        id, "subs", String.valueOf(idProduct));

                                if (buyIntentBundle.getInt("RESPONSE_CODE") == 0) {
                                    PendingIntent pendingIntent = buyIntentBundle.getParcelable("BUY_INTENT");
                                    if (pendingIntent != null) {
                                        startIntentSenderForResult(pendingIntent.getIntentSender(),
                                                1003, new Intent(), Integer.valueOf(0), Integer.valueOf(0),
                                                Integer.valueOf(0));
                                    }
                                }
                            } catch (IntentSender.SendIntentException e) {
                                e.printStackTrace();
                            } catch (RemoteException e) {
                                e.printStackTrace();
                            }
                            mMaterialDialog.dismiss();


                        }
                    })
                    .setNegativeButton("Отклонить", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mMaterialDialog.dismiss();
                        }
                    });

            mMaterialDialog.show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
