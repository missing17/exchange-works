package com.mediaprofi.zukuzyaSecondEdition.ui.myorder;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.json.jackson.JacksonFactory;
import com.mediaprofi.zukuzyaSecondEdition.core.Constants;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

/**
 * Created by aleksei on 12.01.16.
 */
public class MyOrderRequest extends GoogleHttpClientSpiceRequest<MyOrderModel> {

    private String token;

    public MyOrderRequest(String token) {
        super(MyOrderModel.class);
        this.token = token;
    }

    @Override
    public MyOrderModel loadDataFromNetwork() throws Exception {
        String url = Constants.baseURL + "/data_json.php?user_zakaz=1";
        HttpRequest request = getHttpRequestFactory()
                .buildGetRequest(new GenericUrl(url))
                .setHeaders(new HttpHeaders()
                        .set("X-Token", token));
        request.setParser(new JacksonFactory().createJsonObjectParser());
        HttpResponse response = request.execute();
        return response.parseAs(getResultType());
    }
}
