package com.mediaprofi.zukuzyaSecondEdition.ui.settings.problem;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpContent;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.UrlEncodedContent;
import com.google.api.client.json.jackson.JacksonFactory;
import com.mediaprofi.zukuzyaSecondEdition.core.Constants;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by aleksei on 26.01.16.
 */
public class ProblemRequest extends GoogleHttpClientSpiceRequest<ProblemModel> {

    HashMap<String, Object> postParam;

    public ProblemRequest(String title, String body) {
        super(ProblemModel.class);

        postParam = new HashMap<>();
        postParam.put("zag_prob", title);
        postParam.put("text_prob", body);
    }

    @Override
    public ProblemModel loadDataFromNetwork() throws Exception {
        HttpRequest request = null;
        String url = Constants.baseURL + "/zhaloba.php";
        GenericUrl genericUrl = new GenericUrl(url);


        HttpContent content = new UrlEncodedContent(postParam);
        request = buildPostRequest(genericUrl, content);

        request.setParser(new JacksonFactory().createJsonObjectParser());

        HttpResponse response = request.execute();
        return response.parseAs(getResultType());
    }

    private HttpRequest buildPostRequest(GenericUrl genericUrl, HttpContent content) throws IOException {
        System.setProperty("http.keepAlive", "false");
        HttpRequest request = getHttpRequestFactory().buildPostRequest(genericUrl, content);
        customHttpHeader(request);
        return request;
    }

    private void customHttpHeader(HttpRequest request) {
        request.getHeaders().setAcceptEncoding("gzip, deflate");
        request.getHeaders().set("Connection", "close");
        request.getHeaders().setAccept("text/html,application/xml,application/json");
    }
}
