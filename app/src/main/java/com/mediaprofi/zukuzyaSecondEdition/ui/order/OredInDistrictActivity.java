package com.mediaprofi.zukuzyaSecondEdition.ui.order;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mediaprofi.zukuzyaSecondEdition.IClickOnMyOrder;
import com.mediaprofy.zukuzyaSecondEdition.R;
import com.mediaprofi.zukuzyaSecondEdition.core.App;
import com.mediaprofi.zukuzyaSecondEdition.core.BaseSpaceActivity;
import com.mediaprofi.zukuzyaSecondEdition.core.Constants;
import com.mediaprofi.zukuzyaSecondEdition.model.UserModel;
import com.mediaprofi.zukuzyaSecondEdition.ui.description.DescriptionOrder;
import com.mediaprofi.zukuzyaSecondEdition.ui.myorder.decoration.MyOrderDecoration;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.util.List;

/**
 * Created by aleksei on 17.12.15.
 */
public class OredInDistrictActivity extends BaseSpaceActivity implements IClickOnMyOrder {

    private RecyclerView myRecyclerView;
    private LinearLayoutManager linearLayoutManager;
    private RecyclerView.Adapter myOrederAdapter;
    private MyOrderDecoration myOrderDecoration;
    private OrderRequest orderRequest;
    private List<OrderModel.Order> listOrder;

    private RelativeLayout rlWaite;
    private RelativeLayout rlNoData;
    private TextView txtNotyfication;

    @Override
    public void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.oreder_in_disrict);

//        presenter = new PresenterImpl(this);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            myToolbar.setTitleTextColor(getResources().getColor(R.color.white, getApplicationContext().getTheme()));
        } else {
            myToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        }

        SharedPreferences sharedPreferences = getGloabalSetting();
        String token = sharedPreferences.getString(Constants.tokenInSharedPreferences, "");

        UserModel userModel = (UserModel)App.getFromMemoryCache(Constants.keyUser);
        int category = Integer.valueOf(userModel.getCateg_id());

        Intent intent = getIntent();
        orderRequest = new OrderRequest(String.valueOf(intent.getIntExtra(Constants.position, 0)), token);

        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setTitle(intent.getStringExtra(Constants.title));
            ab.setDisplayHomeAsUpEnabled(true);
        }

        myOrderDecoration = new MyOrderDecoration();

        linearLayoutManager = new LinearLayoutManager(getApplicationContext());

        myRecyclerView = (RecyclerView) findViewById(R.id.myRecyclerView);
        if (myRecyclerView != null) {
            myRecyclerView.setLayoutManager(linearLayoutManager);
            myRecyclerView.addItemDecoration(myOrderDecoration);
        }

        rlWaite = (RelativeLayout) findViewById(R.id.rlWaite);
        rlWaite.setVisibility(View.VISIBLE);

        rlNoData = (RelativeLayout) findViewById(R.id.rlNoData);
        txtNotyfication = (TextView) findViewById(R.id.txtNotyfication);
    }

    @Override
    public void onStart() {
        super.onStart();

//        presenter.onSearchClick();

        getSpiceManager().execute(orderRequest, "order", DurationInMillis.ONE_SECOND,
                new OrderRequestListener());
    }


    @Override
    public void clickMyOrder(int position, String title) {
        Intent intentDescription = new Intent(OredInDistrictActivity.this, DescriptionOrder.class);
        intentDescription.putExtra(Constants.isOpenOrder, false);
        intentDescription.putExtra(Constants.positionOrder, position);
        startActivity(intentDescription);
    }

    private void endNetworkRequest(boolean sucess, String text) {
        if (sucess) {
            rlNoData.setVisibility(View.GONE);
            rlWaite.setVisibility(View.GONE);
            myRecyclerView.setVisibility(View.VISIBLE);
        } else {
            rlNoData.setVisibility(View.VISIBLE);
            rlWaite.setVisibility(View.GONE);
            myRecyclerView.setVisibility(View.GONE);
            txtNotyfication.setText(text);
        }
    }

    private void fullList() {
        myOrederAdapter = new OrderAdapter(listOrder, OredInDistrictActivity.this);
        myRecyclerView.setAdapter(myOrederAdapter);
    }

    private class OrderRequestListener implements RequestListener<OrderModel> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            endNetworkRequest(false, getString(R.string.no_data));
        }

        @Override
        public void onRequestSuccess(OrderModel orderModel) {
            listOrder = orderModel.getRajon();
            if (listOrder != null && listOrder.size() != 0) {
                endNetworkRequest(true, null);
                fullList();
            } else {
                endNetworkRequest(false, getString(R.string.data_empty));
            }
        }
    }
}
