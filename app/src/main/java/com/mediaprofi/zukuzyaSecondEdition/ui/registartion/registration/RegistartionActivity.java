package com.mediaprofi.zukuzyaSecondEdition.ui.registartion.registration;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.mediaprofy.zukuzyaSecondEdition.R;
import com.mediaprofi.zukuzyaSecondEdition.core.App;
import com.mediaprofi.zukuzyaSecondEdition.core.BaseSpaceActivity;
import com.mediaprofi.zukuzyaSecondEdition.core.Constants;
import com.mediaprofi.zukuzyaSecondEdition.customView.ChoiseDialogCategory;
import com.mediaprofi.zukuzyaSecondEdition.customView.ChoiseDialogCity;
import com.mediaprofi.zukuzyaSecondEdition.customView.CustomFiled;
import com.mediaprofi.zukuzyaSecondEdition.customView.CustomFiledSpinner;
import com.mediaprofi.zukuzyaSecondEdition.customView.MultyChoiseDialog;
import com.mediaprofi.zukuzyaSecondEdition.customView.UsPhoneNumberFormatter;
import com.mediaprofi.zukuzyaSecondEdition.model.CategoryModel;
import com.mediaprofi.zukuzyaSecondEdition.model.CityModel;
import com.mediaprofi.zukuzyaSecondEdition.model.PodcategoryModel;
import com.mediaprofi.zukuzyaSecondEdition.requests.CategoryRequest;
import com.mediaprofi.zukuzyaSecondEdition.requests.CityRequest;
import com.mediaprofi.zukuzyaSecondEdition.requests.PodcategoryRequest;
import com.mediaprofi.zukuzyaSecondEdition.ui.photoload.IGetPhotoLink;
import com.mediaprofi.zukuzyaSecondEdition.ui.confirm.ConfirmPhone;
import com.mediaprofi.zukuzyaSecondEdition.ui.photoload.PhotoFragment;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.apache.commons.lang3.ArrayUtils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by aleksei on 01.12.15.
 */
public class RegistartionActivity extends BaseSpaceActivity implements IGetPhotoLink {

    private List<CategoryModel.Category> listCategory;
    private List<CityModel.City> listCity;
    private List<PodcategoryModel.Podcategory> listPodcategory;
    private List<Integer> listIdChoise;
    private String idCategory = "";
    private String idCity = "";
    private Context mContext = this;

    private ScrollView scrollView;
    private RelativeLayout rlWaite;
    private RelativeLayout rlNoData;
    private TextView txtNotyfication;

    private CustomFiled phone, password, confirmPassword, name, email;
    private CustomFiledSpinner subCategory;
    private ChoiseDialogCategory dialog;
    private ChoiseDialogCity dialogCity;
    private CustomFiledSpinner category, city;
    private EditText phoneEditText;
    private List<Integer> listError;
    private String photoLink = "";
    private TextView txtLicense;
    private CheckBox checkBox;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            myToolbar.setTitleTextColor(getResources().getColor(R.color.white, getApplicationContext().getTheme()));
        } else {
            myToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        }

        ActionBar ab = getSupportActionBar();
        ab.setTitle(R.string.registarttion);
        ab.setDisplayHomeAsUpEnabled(true);

        checkBox = (CheckBox) findViewById(R.id.checkBox);
        scrollView = (ScrollView) findViewById(R.id.scrollView);
        phone = (CustomFiled) findViewById(R.id.phone);
        password = (CustomFiled) findViewById(R.id.password);
        confirmPassword = (CustomFiled) findViewById(R.id.confirm_password);
        name = (CustomFiled) findViewById(R.id.name);
        email = (CustomFiled) findViewById(R.id.email);

        txtLicense = (TextView) findViewById(R.id.txtLbl);
        txtLicense.setMovementMethod(LinkMovementMethod.getInstance());

        listIdChoise = new ArrayList<>();

        rlWaite = (RelativeLayout) findViewById(R.id.rlWaite);
        rlWaite.setVisibility(View.VISIBLE);
        rlWaite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        phoneEditText = phone.getEditTextView();
        UsPhoneNumberFormatter addLineNumberFormatter = new UsPhoneNumberFormatter(
                new WeakReference<>(phoneEditText));
        phoneEditText.addTextChangedListener(addLineNumberFormatter);


        rlNoData = (RelativeLayout) findViewById(R.id.rlNoData);
        txtNotyfication = (TextView) findViewById(R.id.txtNotyfication);

        Button btnEnter = (Button) findViewById(R.id.btnEnter);
        btnEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (password.getEntreText().length() >= 6 || confirmPassword.getEntreText().length() >= 6) {
                    if (password.getEntreText().equals(confirmPassword.getEntreText()) && phone.getEntreText().length() != 0) {
                        if (idCity.length() != 0) {
                            if (idCategory != null && idCategory.length() != 0) {
                                if (listIdChoise != null && listIdChoise.size() != 0) {
                                    if (checkBox.isChecked())
                                        goOnServerBySMSCode(UsPhoneNumberFormatter.deformatUsNumber(phoneEditText.getText()));
                                    else {
                                        showToast(R.string.you_mat_accept_license);
                                    }
                                } else {
                                    showToast(R.string.subcategory_is_empty);
                                }
                            } else {
                                showToast(R.string.category_is_empty);
                            }
                        } else {
                            showToast(R.string.city_is_empty);
                        }
                    } else {
                        showToast(R.string.password_do_not_match);
                    }
                } else {
                    showToast(R.string.short_password);
                }
            }
        });

        category = (CustomFiledSpinner) findViewById(R.id.category);
        category.setOnClickListener(new View.OnClickListener() {

            DialogInterface.OnClickListener clikc = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    selectCategory(dialog, which);
                }
            };

            @Override
            public void onClick(View v) {
                if (listCategory != null && listCategory.size() != 0) {
                    dialog = new ChoiseDialogCategory();
                    dialog.setList(listCategory);
                    dialog.setClick(clikc);
                    dialog.show(getFragmentManager(), "Tag");
                }
            }
        });


        city = (CustomFiledSpinner) findViewById(R.id.city);
        city.setOnClickListener(new View.OnClickListener() {

            DialogInterface.OnClickListener clikc = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    selectCity(dialog, which);
                }
            };

            @Override
            public void onClick(View v) {
                if (listCity != null && listCity.size() != 0) {
                    dialogCity = new ChoiseDialogCity();
                    dialogCity.setList(listCity);
                    dialogCity.setClick(clikc);
                    dialogCity.show(getFragmentManager(), "Tag");
                }
            }
        });

        MultiChoisShowClickListener multiChoiseClickListener = new MultiChoisShowClickListener();
        subCategory = (CustomFiledSpinner) findViewById(R.id.subcategory);
        subCategory.setOnClickListener(multiChoiseClickListener);

        Fragment photoFragment = new PhotoFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(R.id.photoFragment, photoFragment);
        ft.commit();
    }

    private void selectCity(DialogInterface dialog, int which) {
        city.setTextOnTextView(listCity.get(which).getName());
        idCity = listCity.get(which).getId();
        dialog.dismiss();
    }

    private void selectCategory(DialogInterface dialog, int which) {
        category.setTextOnTextView(listCategory.get(which).getName_kategor());
        idCategory = listCategory.get(which).getId();
        dialog.dismiss();

        goOnSreverByPodcategory();
    }

    private void goOnSreverByPodcategory() {
        rlWaite.setVisibility(View.VISIBLE);
        PodcategoryRequest podcategoryRequest = new PodcategoryRequest(idCategory);
        getSpiceManager().execute(podcategoryRequest, new PodcategoryRequestListener());
    }

    private void goOnServerBySMSCode(String phone) {
        SMSCodeRequest smsCodeRequest = new SMSCodeRequest(phone);
        getSpiceManager().execute(smsCodeRequest, new SmsRequestListener());
    }


    @Override
    public void onResume() {
        super.onResume();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    public void onStart() {
        super.onStart();
        checkBaseInfo();
    }

    private void checkBaseInfo() {
        listError = new ArrayList<>();
        listCategory = (List<CategoryModel.Category>) App.getFromMemoryCache(Constants.keyCategory);
        listCity = (List<CityModel.City>) App.getFromMemoryCache(Constants.keyCity);

        if (listCategory == null || listCategory.size() == 0)
            listError.add(2);

        if (listCity == null || listCity.size() == 0)
            listError.add(1);

        if (listError.size() != 0)
            goOnServerByCategory(listError);
        else
            endNetworkRequest(true, null);
    }

    private void goOnServerByCategory(List<Integer> listError) {
        for (int id : listError) {
            if (id == 2) {
                CategoryRequest categoryRequest = new CategoryRequest();
                getSpiceManager().execute(categoryRequest, new CategoryRequestListener());
            }
            if (id == 1) {
                CityRequest cityRequest = new CityRequest();
                getSpiceManager().execute(cityRequest, new CityRequestListener());
            }
        }
    }

    private void checkLoadAllData() {
        if (listError.size() == 0)
            endNetworkRequest(true, null);
    }

    private void updateSubcategoryFields(List<Integer> lisyIdChoise) {
        StringBuilder strBuilder = new StringBuilder();

        for (int i = 0; i < lisyIdChoise.size(); i++) {
            int id = lisyIdChoise.get(i);
            strBuilder.append(listPodcategory.get(id).getName_podkategor());

            if (i != (lisyIdChoise.size() - 1))
                strBuilder.append(" ,");
        }

        subCategory.setTextOnTextView(strBuilder.toString());
    }

    private void endNetworkRequest(boolean sucess, String text) {
        if (sucess) {
            rlNoData.setVisibility(View.GONE);
            rlWaite.setVisibility(View.GONE);
            scrollView.setVisibility(View.VISIBLE);
        } else {
            rlNoData.setVisibility(View.VISIBLE);
            rlWaite.setVisibility(View.GONE);
            scrollView.setVisibility(View.GONE);
            txtNotyfication.setText(text);
        }
    }

    @Override
    public void useLinkOnPhoto(String link) {
        if (link.length() != 0) {
            photoLink = link;
        } else {
            showToast(R.string.error_load_foto);
        }
    }

    private class SmsRequestListener implements RequestListener<SMSCodeModel> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            endNetworkRequest(true, null);
            showToast(R.string.no_data);
        }

        @Override
        public void onRequestSuccess(SMSCodeModel smsCodeModel) {
            codeReseiveGoOnNextActivity(smsCodeModel.getKode());
        }
    }

    private void codeReseiveGoOnNextActivity(Integer code) {
        Integer[] tmpSubCategoryArray = new Integer[listIdChoise.size()];
        tmpSubCategoryArray = listIdChoise.toArray(tmpSubCategoryArray);

        int[] tmpSubCategory = new int[listIdChoise.size()];
        for (int i = 0; i < listIdChoise.size(); i++) {
            tmpSubCategory[i] = Integer.valueOf(listPodcategory.get(listIdChoise.get(i)).getId());
        }

        int[] subCategory = ArrayUtils.toPrimitive(listIdChoise.toArray(tmpSubCategoryArray));

        Intent confirmPhoneIntent = new Intent(RegistartionActivity.this, ConfirmPhone.class);
        confirmPhoneIntent.putExtra(Constants.name, name.getEntreText());
        confirmPhoneIntent.putExtra(Constants.password, password.getEntreText());
        confirmPhoneIntent.putExtra(Constants.phone, UsPhoneNumberFormatter.deformatUsNumber(phoneEditText.getText()));
        confirmPhoneIntent.putExtra(Constants.email, email.getEntreText());
        confirmPhoneIntent.putExtra(Constants.city, idCity);
        confirmPhoneIntent.putExtra(Constants.category, idCategory);
        confirmPhoneIntent.putExtra(Constants.subcategory, tmpSubCategory);
        confirmPhoneIntent.putExtra(Constants.smscode, code);
        confirmPhoneIntent.putExtra(Constants.photo, photoLink);

        confirmPhoneIntent.setFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);

        startActivity(confirmPhoneIntent);
    }

    private class CityRequestListener implements RequestListener<CityModel> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            endNetworkRequest(true, null);
            showToast(R.string.no_data);
        }

        @Override
        public void onRequestSuccess(CityModel cityModel) {
            App.putInMemoryCache(Constants.keyCity, cityModel.getCity());
            listCity = cityModel.getCity();
            listError.remove(0);
            checkLoadAllData();
        }
    }

    private class PodcategoryRequestListener implements RequestListener<PodcategoryModel> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            endNetworkRequest(true, null);
            showToast(R.string.cant_load_subCategory);
        }

        @Override
        public void onRequestSuccess(PodcategoryModel podcategoryModel) {
            App.putInMemoryCache(Constants.keyPodcategory, podcategoryModel.getSubCategory());
            listPodcategory = podcategoryModel.getSubCategory();
            endNetworkRequest(true, null);
        }
    }

    private class CategoryRequestListener implements RequestListener<CategoryModel> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            endNetworkRequest(true, null);
            showToast(R.string.no_data);
        }

        @Override
        public void onRequestSuccess(CategoryModel categoryModel) {
            App.putInMemoryCache(Constants.keyCategory, categoryModel.getKategor());
            listCategory = categoryModel.getKategor();
            listError.remove(0);
            checkLoadAllData();
        }
    }

    public interface MultiChoiseCallBack {
        void getChoiseItem(List<Integer> lisyIdChoise);
    }

    private class MultiChoisShowClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if (listPodcategory != null && listPodcategory.size() != 0) {
                MultyChoiseDialog dialog = new MultyChoiseDialog();
                dialog.setList(listPodcategory);
                dialog.setClick(new PodacategoryChoise());
                dialog.show(getFragmentManager(), "Tag");
            }
        }
    }

    private class PodacategoryChoise implements MultiChoiseCallBack {
        @Override
        public void getChoiseItem(List<Integer> lisyIdChoise) {
            listIdChoise = lisyIdChoise;
            updateSubcategoryFields(lisyIdChoise);
        }
    }
}
