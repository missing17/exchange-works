package com.mediaprofi.zukuzyaSecondEdition.ui.settings;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.mediaprofy.zukuzyaSecondEdition.R;
import com.mediaprofi.zukuzyaSecondEdition.core.BaseSpaceActivity;
import com.mediaprofi.zukuzyaSecondEdition.ui.settings.changepswd.ChangePassword;
import com.mediaprofi.zukuzyaSecondEdition.ui.settings.notification.NotifycationActivity;
import com.mediaprofi.zukuzyaSecondEdition.ui.settings.problem.ProblemActivity;

/**
 * Created by aleksei on 16.12.15.
 */
public class Setting extends BaseSpaceActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);

        buildActionBar(R.string.settingsUp, true, false);

        RelativeLayout changePassword = (RelativeLayout) findViewById(R.id.changePassword);
        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent changePassword = new Intent(Setting.this, ChangePassword.class);
                startActivity(changePassword);
            }
        });

        RelativeLayout rlNotify = (RelativeLayout) findViewById(R.id.notify);
        rlNotify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent notify = new Intent(Setting.this, NotifycationActivity.class);
                startActivity(notify);
            }
        });

        RelativeLayout rlProble = (RelativeLayout) findViewById(R.id.reportProblem);
        rlProble.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent problem = new Intent(Setting.this, ProblemActivity.class);
                startActivity(problem);
            }
        });
    }
}
