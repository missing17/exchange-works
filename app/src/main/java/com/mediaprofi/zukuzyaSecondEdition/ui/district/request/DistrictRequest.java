package com.mediaprofi.zukuzyaSecondEdition.ui.district.request;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.json.jackson.JacksonFactory;
import com.mediaprofi.zukuzyaSecondEdition.core.Constants;
import com.mediaprofi.zukuzyaSecondEdition.ui.district.model.DistrictModel;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

import java.io.IOException;

/**
 * Created by aleksei on 24.12.15.
 */
public class DistrictRequest extends GoogleHttpClientSpiceRequest<DistrictModel> {

    private int city;
    private String token;

    public DistrictRequest(int city, String token) {
        super(DistrictModel.class);
        this.city = city;
        this.token = token;
    }

    @Override
    public DistrictModel loadDataFromNetwork() throws IOException {
        String url = Constants.baseURL + "/rajon.php?city=" + city;
        HttpRequest request = getHttpRequestFactory()
                .buildGetRequest(new GenericUrl(url))
                .setHeaders(new HttpHeaders()
                        .set("X-Token", token));
        request.setParser(new JacksonFactory().createJsonObjectParser());
        return request.execute().parseAs(getResultType());
    }

}
