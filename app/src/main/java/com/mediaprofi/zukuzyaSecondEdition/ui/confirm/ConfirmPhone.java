package com.mediaprofi.zukuzyaSecondEdition.ui.confirm;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mediaprofi.zukuzyaSecondEdition.MainActivity;
import com.mediaprofy.zukuzyaSecondEdition.R;
import com.mediaprofi.zukuzyaSecondEdition.core.BaseSpaceActivity;
import com.mediaprofi.zukuzyaSecondEdition.core.Constants;
import com.mediaprofi.zukuzyaSecondEdition.customView.CustomFiled;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

/**
 * Created by aleksei on 15.12.15.
 */
public class ConfirmPhone extends BaseSpaceActivity {

    private Context mContext;

    private CustomFiled edtText;
    private RelativeLayout rlWaite;

    private String name;
    private String phone;
    private String mail;
    private String password;
    private String category;
    private String subCatForRequest;
    private String city;
    private String photo;

    @Override
    public void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.confirm_phone);
        mContext = this;

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            myToolbar.setTitleTextColor(getResources().getColor(R.color.white, getApplicationContext().getTheme()));
        } else {
            myToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        }

        ActionBar ab = getSupportActionBar();
        ab.setTitle(R.string.confirm_number);
        ab.setDisplayHomeAsUpEnabled(true);

        rlWaite = (RelativeLayout) findViewById(R.id.rlWaite);

        name = getIntent().getStringExtra(Constants.name);
        phone = "7" + getIntent().getStringExtra(Constants.phone);
        photo = getIntent().getStringExtra(Constants.photo);
        mail = getIntent().getStringExtra(Constants.email);
        password = getIntent().getStringExtra(Constants.password);
        category = getIntent().getStringExtra(Constants.category);
        city = getIntent().getStringExtra(Constants.city);
        final int smsCode = getIntent().getIntExtra(Constants.smscode, 0);
        int[] subCategory = getIntent().getIntArrayExtra(Constants.subcategory);

        StringBuilder strBuilder = new StringBuilder();

        for (int i = 0; i < subCategory.length; i++) {
            strBuilder.append(String.valueOf(subCategory[i]));

            if (i != (subCategory.length - 1))
                strBuilder.append(" ,");
        }

        subCatForRequest = strBuilder.toString();

        edtText = (CustomFiled) findViewById(R.id.confirmCode);

        Button btnConfirm = (Button) findViewById(R.id.btnConfirm);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtText.getEntreText().length() != 0) {
                    int userCode = Integer.valueOf(edtText.getEntreText());
                    if (smsCode == userCode) {
                        sendAllDataOnServer();
                    } else {
                        showToast(R.string.smscode_do_not_match);
                    }
                }
            }
        });

    }

    private void sendAllDataOnServer() {
        rlWaite.setVisibility(View.VISIBLE);
        SendAllDataRequest sendAllDataRequest = new SendAllDataRequest(name, phone, mail, password, category, subCatForRequest, city, photo);
        getSpiceManager().execute(sendAllDataRequest, new SendAllDataRequestListener());
    }

    private void startMainActivity() {
        Intent mainIntent = new Intent(ConfirmPhone.this, MainActivity.class);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(mainIntent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            super.onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }


    private class SendAllDataRequestListener implements RequestListener<RegTokenModel> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            rlWaite.setVisibility(View.GONE);
            showToast(R.string.error_load_to_server);
        }

        @Override
        public void onRequestSuccess(RegTokenModel regTokenModel) {

            if (regTokenModel.getAutoriz().equals("yes")) {
                SharedPreferences sharedPreferences = getSharedPreferences(Constants.nameSharedPrefernces, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(Constants.tokenInSharedPreferences, regTokenModel.getToken());
                editor.putInt(Constants.cityInSharedPreferences, Integer.valueOf(city));
                editor.apply();

                rlWaite.setVisibility(View.GONE);
                startMainActivity();
            } else {
                rlWaite.setVisibility(View.GONE);
                Toast.makeText(mContext, regTokenModel.getToken(), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
