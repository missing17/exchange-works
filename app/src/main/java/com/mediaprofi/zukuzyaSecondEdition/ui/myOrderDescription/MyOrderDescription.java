package com.mediaprofi.zukuzyaSecondEdition.ui.myOrderDescription;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.vending.billing.IInAppBillingService;
import com.bumptech.glide.Glide;
import com.mediaprofi.zukuzyaSecondEdition.core.App;
import com.mediaprofi.zukuzyaSecondEdition.core.BaseSpaceActivity;
import com.mediaprofi.zukuzyaSecondEdition.core.Constants;
import com.mediaprofi.zukuzyaSecondEdition.model.UserModel;
import com.mediaprofi.zukuzyaSecondEdition.ui.description.done.DoneRequest;
import com.mediaprofi.zukuzyaSecondEdition.ui.description.refuse.RefuseRequest;
import com.mediaprofy.zukuzyaSecondEdition.R;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;


/**
 * Created by aleksei on 16.12.15.
 */
public class MyOrderDescription extends BaseSpaceActivity {

    private Context mContext;
    private Button btnDone, btnRefuse;
    private LinearLayout containerForButtons;
    //    private DescriptionOrderRequest descriptionRequest;
    private RelativeLayout rlWaite;
    private RelativeLayout rlNoData;
    private TextView txtNotyfication;
    private ScrollView myScrollView;
    private Bundle bundle;

    private IInAppBillingService mService;

    private TextView subject;
    private TextView price;
    private TextView categoty, city, adress, area;
    private TextView fullDescription, nameCustomer, phoneCustomer;
    private TextView datePresent;
    private int idOrder;
    private String token;
    private ImageView foto1, foto2, foto3, foto4;

    @Override
    public void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.description_myoreder);

        mContext = this;
        Intent serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        serviceIntent.setPackage("com.android.vending");
        bindService(serviceIntent, mServiceConnection, Context.BIND_AUTO_CREATE);

        subject = (TextView) findViewById(R.id.subject);
        price = (TextView) findViewById(R.id.price);
        categoty = (TextView) findViewById(R.id.category);
        city = (TextView) findViewById(R.id.city);
        adress = (TextView) findViewById(R.id.adress);
        area = (TextView) findViewById(R.id.area);
        fullDescription = (TextView) findViewById(R.id.fullDescription);
        nameCustomer = (TextView) findViewById(R.id.nameCustomer);
        phoneCustomer = (TextView) findViewById(R.id.phoneNumber);
        datePresent = (TextView) findViewById(R.id.datePresent);
        foto1 = (ImageView) findViewById(R.id.foto1);
        foto2 = (ImageView) findViewById(R.id.foto2);
        foto3 = (ImageView) findViewById(R.id.foto3);
        foto4 = (ImageView) findViewById(R.id.foto4);

        rlWaite = (RelativeLayout) findViewById(R.id.rlWaite);
        rlWaite.setVisibility(View.VISIBLE);

        rlNoData = (RelativeLayout) findViewById(R.id.rlNoData);
        txtNotyfication = (TextView) findViewById(R.id.txtNotyfication);

        myScrollView = (ScrollView) findViewById(R.id.myScrollView);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            myToolbar.setTitleTextColor(getResources().getColor(R.color.white, getApplicationContext().getTheme()));
        } else {
            myToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        }

        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setTitle("");

        bundle = getIntent().getExtras();
        idOrder = bundle.getInt(Constants.positionOrder);

        SharedPreferences sharedPreferences = getSharedPreferences(Constants.nameSharedPrefernces, MODE_PRIVATE);
        token = sharedPreferences.getString(Constants.tokenInSharedPreferences, "");


        btnDone = (Button) findViewById(R.id.done);
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderDone(idOrder);
            }
        });

        btnRefuse = (Button) findViewById(R.id.refuse);
        btnRefuse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderRefuse(idOrder);
            }
        });

        containerForButtons = (LinearLayout) findViewById(R.id.doneOrRefuse);
        containerForButtons.setVisibility(View.VISIBLE);

    }

    private void orderDone(int idOrder) {
        DoneRequest doneRequest = new DoneRequest(token, String.valueOf(idOrder));
        getSpiceManager().execute(doneRequest, new DoneRequestListener());
    }

    private void orderRefuse(int idOrder) {
        RefuseRequest refuseRequest = new RefuseRequest(token, String.valueOf(idOrder));
        getSpiceManager().execute(refuseRequest, new RefizeRequestListener());
    }

    @Override
    public void onStart() {
        super.onStart();
        MyOrderDescriptionRequest descriptionRequest = new MyOrderDescriptionRequest(idOrder,token);
        getSpiceManager().execute(descriptionRequest, new DescriptionListener());
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        bundle = intent.getBundleExtra("Info");
    }

    ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = IInAppBillingService.Stub.asInterface(service);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mService != null) {
            unbindService(mServiceConnection);
        }
    }

    private void fullingLayout(MyOrderDescriptionModel descriptionModel) {

        categoty.setText(descriptionModel.getKateg());
        if (descriptionModel.getPrice().length() != 0)
            price.setText(descriptionModel.getPrice() + " руб.");

        subject.setText(descriptionModel.getPodkateg());
        city.setText(descriptionModel.getCity());
        area.setText(descriptionModel.getRajon());

        adress.setText(descriptionModel.getStreet());

        if (descriptionModel.getDom().length() != 0)
            adress.append(" д. " + descriptionModel.getDom());

        fullDescription.setText(descriptionModel.getOpis());
        nameCustomer.setText(descriptionModel.getName());
        phoneCustomer.setText(descriptionModel.getPhone()); // здесь


        datePresent.setText(descriptionModel.getDate_reg());

        if (descriptionModel.getStatus() != null && descriptionModel.getStatus().equals("Выполнено")) {
            containerForButtons.setVisibility(View.GONE);
        } else {
            containerForButtons.setVisibility(View.VISIBLE);
        }

        if (descriptionModel.getFoto1() != null && descriptionModel.getFoto1().length() != 0) {
            foto1.setVisibility(View.VISIBLE);
            Glide.with(this).load(descriptionModel.getFoto1()).into(foto1);
        }

        if (descriptionModel.getFoto2() != null && descriptionModel.getFoto2().length() != 0) {
            foto2.setVisibility(View.VISIBLE);
            Glide.with(this).load(descriptionModel.getFoto2()).into(foto2);
        }

        if (descriptionModel.getFoto3() != null && descriptionModel.getFoto3().length() != 0) {
            foto3.setVisibility(View.VISIBLE);
            Glide.with(this).load(descriptionModel.getFoto3()).into(foto3);
        }

        if (descriptionModel.getFoto4() != null && descriptionModel.getFoto4().length() != 0) {
            foto4.setVisibility(View.VISIBLE);
            Glide.with(this).load(descriptionModel.getFoto4()).into(foto4);
        }
    }


    private void endNetworkRequest(boolean sucess, String text) {
        if (sucess) {
            rlNoData.setVisibility(View.GONE);
            rlWaite.setVisibility(View.GONE);
            myScrollView.setVisibility(View.VISIBLE);
        } else {
            rlNoData.setVisibility(View.VISIBLE);
            rlWaite.setVisibility(View.GONE);
            myScrollView.setVisibility(View.GONE);
            txtNotyfication.setText(text);
        }
    }


    private class DescriptionListener implements RequestListener<MyOrderDescriptionModel> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            endNetworkRequest(false, getString(R.string.no_data));
        }

        @Override
        public void onRequestSuccess(MyOrderDescriptionModel descriptionOrderModel) {
            endNetworkRequest(true, null);
            fullingLayout(descriptionOrderModel);
        }
    }


    private class RefizeRequestListener implements RequestListener<UserModel> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            Log.e("de", "de");
        }

        @Override
        public void onRequestSuccess(UserModel refuseModel) {
            App.putInMemoryCache(Constants.keyUser, refuseModel);
            finish();
        }
    }

    private class DoneRequestListener implements RequestListener<UserModel> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            Log.e("de", "de");
        }

        @Override
        public void onRequestSuccess(UserModel userModel) {
            App.putInMemoryCache(Constants.keyUser, userModel);
            containerForButtons.setVisibility(View.GONE);
        }
    }

}
