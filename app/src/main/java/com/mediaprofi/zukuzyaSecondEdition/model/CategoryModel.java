package com.mediaprofi.zukuzyaSecondEdition.model;

import com.google.api.client.util.Key;

import java.util.List;

/**
 * Created by aleksei on 12.01.16.
 */
public class CategoryModel {

    @Key
    private List<Category> kategor;

    public List<Category> getKategor() {
        return kategor;
    }

    public void setKategor(List<Category> kategor) {
        this.kategor = kategor;
    }

    public static class Category {
        @Key
        private String id;

        @Key
        private String name_kategor;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName_kategor() {
            return name_kategor;
        }

        public void setName_kategor(String name_kategor) {
            this.name_kategor = name_kategor;
        }
    }
}
