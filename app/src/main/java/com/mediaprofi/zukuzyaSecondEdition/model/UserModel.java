package com.mediaprofi.zukuzyaSecondEdition.model;


import com.google.api.client.util.Key;

/**
 * Created by aleksei on 11.01.16.
 */
public class UserModel {

//
    /**
     * ﻿{"name":"\u0410\u043b\u0435\u043a\u0441\u0435\u0439",
     * "mail":"new_514036@mail.ru",
     * "phone":"79033333333",
     * "photo":null,"categ_id":"58",
     * "categ_name":"IT,\u0438\u043d\u0442\u0435\u0440\u043d\u0435\u0442,\u0442\u0435\u043b\u0435\u043a\u043e\u043c",
     * "categ_pod_id":["64","61"],
     * "categ_pod_name":["\u0410\u0434\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0438\u0440\u043e\u0432\u0430\u043d\u0438\u0435 \u0441\u0430\u0439\u0442\u0430","\u0410\u0443\u0434\u0438\u0442 \u0441\u0430\u0439\u0442\u0430"],
     * "city":"\u0423\u0444\u0430","city_id":"45",
     * "date_podpis":""}
     */
//
    @Key
    private String name;

    @Key
    private String mail;

    @Key
    private String phone;

    @Key
    private String photo;

    @Key
    private String categ_id;

    @Key
    private String categ_name;

    @Key
    private String[] categ_pod_id;

    @Key
    private String[] categ_pod_name;

    @Key
    private String city;

    @Key
    private String city_id;

    @Key
    private String date_podpis;

    @Key
    private Integer est_zakaz;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity_id() {

        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getCateg_id() {
        return categ_id;
    }

    public void setCateg_id(String categ_id) {
        this.categ_id = categ_id;
    }

    public String getCateg_name() {
        return categ_name;
    }

    public void setCateg_name(String categ_name) {
        this.categ_name = categ_name;
    }

    public String[] getCateg_pod_id() {
        return categ_pod_id;
    }

    public void setCateg_pod_id(String[] categ_pod_id) {
        this.categ_pod_id = categ_pod_id;
    }

    public String[] getCateg_pod_name() {
        return categ_pod_name;
    }

    public void setCateg_pod_name(String[] categ_pod_name) {
        this.categ_pod_name = categ_pod_name;
    }

    public String getDate_podpis() {
        return date_podpis;
    }

    public void setDate_podpis(String date_podpis) {
        this.date_podpis = date_podpis;
    }

    public Integer getEst_zakaz() {
        return est_zakaz;
    }

    public void setEst_zakaz(Integer est_zakaz) {
        this.est_zakaz = est_zakaz;
    }
}
