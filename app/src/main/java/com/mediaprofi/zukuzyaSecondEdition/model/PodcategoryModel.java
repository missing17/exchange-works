package com.mediaprofi.zukuzyaSecondEdition.model;

import com.google.api.client.util.Key;

import java.util.List;

/**
 * Created by aleksei on 16.01.16.
 */
public class PodcategoryModel {

    @Key
    private List<Podcategory> subCategory;

    public List<Podcategory> getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(List<Podcategory> subCategory) {
        this.subCategory = subCategory;
    }

    public static class Podcategory{

        @Key
        private String id;

        @Key
        private String name_podkategor;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName_podkategor() {
            return name_podkategor;
        }

        public void setName_podkategor(String name_podkategor) {
            this.name_podkategor = name_podkategor;
        }
    }
}
