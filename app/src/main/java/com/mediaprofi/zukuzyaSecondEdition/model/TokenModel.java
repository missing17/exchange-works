package com.mediaprofi.zukuzyaSecondEdition.model;

import com.google.api.client.util.Key;

/**
 * Created by aleksei on 06.01.16.
 */
public class TokenModel {

    @Key
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
