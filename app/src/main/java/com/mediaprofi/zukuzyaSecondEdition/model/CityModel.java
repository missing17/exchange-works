package com.mediaprofi.zukuzyaSecondEdition.model;

import com.google.api.client.util.Key;

import java.util.List;

/**
 * Created by aleksei on 19.01.16.
 */
public class CityModel {

    @Key
    private List<City> city;

    public List<City> getCity() {
        return city;
    }

    public void setCity(List<City> city) {
        this.city = city;
    }

    public static class City{

        @Key
        private String id;

        @Key
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
